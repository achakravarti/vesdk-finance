/*
 * db/pg.c -- implementation of PostgreSQL interface.
 * This file is part of VE SDK Finance Microservice.
 *
 * SDPX-License-Identifier: ISC
 * Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */


#include "db.h"



/*
 * Opens a connection to the vefin_test test database for a stack allocated
 * struct db database handle. See also:
 *   - vefindb(3)
 *   - https://www.postgresql.org/docs/current/libpq-connect.html
 */
void
pg_new(struct pg *db)
{
	const char *ERR = "ED01 - pg_new()";
	const char *CONNSTR = "user=_vefin_test password=_vefin_test"
		              " dbname=vefin_test";

	assert(db);
	db->res = NULL;
	db->conn = PQconnectdb(CONNSTR);

	if (PQstatus(db->conn) == CONNECTION_BAD)
		err_fatal(ECONNREFUSED, ERR);
}


/*
 * Closes the connection to vefin_test database for a stack allocated struct db
 * database handle. See also:
 *   - vefindb(3)
 *   - https://www.postgresql.org/docs/current/libpq-connect.html
 */
void
pg_free(struct pg *db)
{
	assert(db);
	
	if (db->res)
		PQclear(db->res);

	if (db->conn)
		PQfinish(db->conn);
}


/*
 * Executes a prepared statement through the open connection in a struct db
 * database handle. See also:
 *   - vefindb(3)
 *   - https://www.postgresql.org/docs/current/libpq-exec.html
 */
const char *
pg_exec(struct pg *db, const char *stmt, const char *argv[])
{
        register int   argc;
        register char *s;

        assert(db);
	assert(db->conn);
	assert(stmt);

	if (db->res) {
		PQclear(db->res);
		db->res = NULL;
	}

	if (argv) {
		s = (char *)stmt;
		argc = 0;
		while ((s = strstr(stmt, "$"))) {
			s++;
			argc++;
		}

		db->res = PQexecParams(db->conn, stmt, argc, NULL, argv, NULL,
				       NULL, 0);
	} else
		db->res = PQexec(db->conn, stmt);

	if (PQresultStatus(db->res) == PGRES_TUPLES_OK)
		return PQgetvalue(db->res, 0, 0);

	return NULL;
}
