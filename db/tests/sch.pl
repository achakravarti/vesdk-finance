#! /usr/bin/perl
# -*- mode: perl -*-
#
# db/tests/sch.pl -- tests for database schema.
# This file is part of VE SDK Finance Microservice.
# See vesdk.finance.db(3), vesdk.finance.db.ref(3) for documentation.
#
# SDPX-License-Identifier: ISC
# Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#


use 5.32.0;

use File::Basename;
use lib dirname(__FILE__);
require 'util.pl';

use Test::More tests => 3;


test_setup();

test_schema_exists('vefin');
test_extension_exists('plpgsql');
test_extension_exists('pg_trgm');

test_teardown();
