#! /usr/bin/perl
# -*- mode: perl -*-
#
# db/tests/dom.pl -- tests for domains.
# This file is part of VE SDK Finance Microservice.
#
# SDPX-License-Identifier: ISC
# Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#

use 5.32.0;

use File::Basename;
use lib dirname(__FILE__);
require 'util.pl';

use Test::More tests => 115;


#
# Utility function to test whether a given string in the required format is
# accepted by a vefin domain.
#
sub domain_constraint_pass
{
    my ($dom, $val) = @_;
    my $test = "Domain $dom accepts $val";
    my $sql = "SELECT ?::vefin.$dom;";

    my $sth = our $DBH->prepare($sql) or die $DBH->errstr;
    $sth->execute($val);
    $sth->err ? fail($test) : pass($test);

    $sth->finish();
}


#
# Utility function to test whether a given string not matching the required
# format is rejected by a vefin domain. Since this is testing for a failing
# condition, the SQL query needs to wrapped within a transaction block to allow
# the subsequent tests to continue.
#
sub domain_constraint_fail
{
    my ($dom, $val) = @_;
    my $test = "Domain vefin.$dom rejects $val";
    my $sql = 'DO $$ BEGIN SELECT ?::vefin.' . "$dom" . '; END; $$;';

    my $sth = our $DBH->prepare($sql) or die $DBH->errstr;
    $sth->execute($val);
    $sth->err ? pass($test) : fail($test);

    $sth->finish();
}


#
# Utility function to run all the tests pertinent to the page domain.
#
sub page_domain_test
{
    test_domain_exists('page');

    domain_constraint_pass('page', '1,1');
    domain_constraint_pass('page', '1,3');
    domain_constraint_pass('page', '99,99');
    domain_constraint_pass('page', '990,19');
    domain_constraint_pass('page', '223440990,192343432344');

    domain_constraint_fail('page', '0,1');
    domain_constraint_fail('page', '-1,1');
    domain_constraint_fail('page', '-1234,1');
    domain_constraint_fail('page', 'a,1');
    domain_constraint_fail('page', '.,1');
    domain_constraint_fail('page', 'abc,1');
    domain_constraint_fail('page', '%,1');
    domain_constraint_fail('page', '1,0');
    domain_constraint_fail('page', '1,-1');
    domain_constraint_fail('page', '1,-1234');
    domain_constraint_fail('page', '1,a');
    domain_constraint_fail('page', '1,.');
    domain_constraint_fail('page', '1,abc');
    domain_constraint_fail('page', '1,%');
    domain_constraint_fail('page', '');
    domain_constraint_fail('page', ',');
    domain_constraint_fail('page', ',,');
    domain_constraint_fail('page', ', ,');
    domain_constraint_fail('page', '1, 1');
    domain_constraint_fail('page', '1 ,1');
    domain_constraint_fail('page', '1 , 1');
    domain_constraint_fail('page', '1,1 ');
    domain_constraint_fail('page', ' 1,1');
    domain_constraint_fail('page', ' 1,1 ');
    domain_constraint_fail('page', ' 1, 1 ');
    domain_constraint_fail('page', '15t,1');
    domain_constraint_fail('page', 't515,1');
    domain_constraint_fail('page', '5t5,1');
    domain_constraint_fail('page', '22');
}


#
# Utility function to run the rank domain tests.
#
sub rank_domain_test
{
    test_domain_exists('rank');

    domain_constraint_pass('rank', '1,-1');
    domain_constraint_pass('rank', '1,0');
    domain_constraint_pass('rank', '1,1');
    domain_constraint_pass('rank', '5,-1');
    domain_constraint_pass('rank', '7889,0');
    domain_constraint_pass('rank', '112343434334,1');

    domain_constraint_fail('rank', '0,1');
    domain_constraint_fail('rank', '-1,1');
    domain_constraint_fail('rank', '-1234,1');
    domain_constraint_fail('rank', 'a,1');
    domain_constraint_fail('rank', '.,1');
    domain_constraint_fail('rank', 'abc,1');
    domain_constraint_fail('rank', '%,1');
    domain_constraint_fail('rank', '1,0');
    domain_constraint_fail('rank', '1,2');
    domain_constraint_fail('rank', '1,-1234');
    domain_constraint_fail('rank', '1,a');
    domain_constraint_fail('rank', '1,.');
    domain_constraint_fail('rank', '1,abc');
    domain_constraint_fail('rank', '1,%');
    domain_constraint_fail('rank', '');
    domain_constraint_fail('rank', ',');
    domain_constraint_fail('rank', ',,');
    domain_constraint_fail('rank', ', ,');
    domain_constraint_fail('rank', '1, 1');
    domain_constraint_fail('rank', '1 ,1');
    domain_constraint_fail('rank', '1 , 1');
    domain_constraint_fail('rank', '1,1 ');
    domain_constraint_fail('rank', ' 1,1');
    domain_constraint_fail('rank', ' 1,1 ');
    domain_constraint_fail('rank', ' 1, 1 ');
    domain_constraint_fail('rank', '15t,1');
    domain_constraint_fail('rank', 't515,1');
    domain_constraint_fail('rank', '5t5,1');
    domain_constraint_fail('rank', '20');
}


#
# Utility function to run the filter domain tests.
#
sub filter_domain_test
{
    test_domain_exists('filter');

    domain_constraint_pass('filter', 'a');
    domain_constraint_pass('filter', 'A');
    domain_constraint_pass('filter', 'a b');
    domain_constraint_pass('filter', 'A b c');
    domain_constraint_pass('filter', 'abc');
    domain_constraint_pass('filter', 'ABC%');
    domain_constraint_pass('filter', '1');
    domain_constraint_pass('filter', '123');
    domain_constraint_pass('filter', '123abc');
    domain_constraint_pass('filter', '"123ABC"');
    domain_constraint_pass('filter', '!@~');
    domain_constraint_pass('filter', "'hello!'");
    domain_constraint_pass('filter', 'বাংলা');
    domain_constraint_pass('filter', '"বাংলা!"');
    domain_constraint_pass('filter', '!বাংলা');
    domain_constraint_pass('filter', "'বাংলা'");
    domain_constraint_pass('filter', 'a বাং লা');
    domain_constraint_pass('filter', 'বাং  লা');
    domain_constraint_pass('filter', 'বাং   লা');
    domain_constraint_pass('filter', 'বাংলা a');
    domain_constraint_pass('filter', 'aবাং লা');
    domain_constraint_pass('filter', 'hel lo!');
    domain_constraint_pass('filter', 'hel  lo!');
    domain_constraint_pass('filter', 'he ll o!');
    domain_constraint_pass('filter', 'h  ello!');
    domain_constraint_pass('filter', '%h e l l o!');

    domain_constraint_fail('filter', '');
    domain_constraint_fail('filter', ' ');
    domain_constraint_fail('filter', '  ');
    domain_constraint_fail('filter', ' a');
    domain_constraint_fail('filter', 'a ');
    domain_constraint_fail('filter', ' a ');
    domain_constraint_fail('filter', ' % ');
    domain_constraint_fail('filter', ' abc def ');
    domain_constraint_fail('filter', ' বাংলা');
    domain_constraint_fail('filter', 'বাংলা ');
    domain_constraint_fail('filter', ' বাংলা ');
    domain_constraint_fail('filter', ' aবাংলা');
    domain_constraint_fail('filter', 'aবাংলা ');
    domain_constraint_fail('filter', ' aবাংলাa ');
}


#
# Setup and run the domain tests, tearing down when done.
#

test_setup();

page_domain_test();
rank_domain_test();
filter_domain_test();


#
# Tests for vefin._util_pgck()
#
my $p = 'p_ini integer, p_len integer, p_col integer, p_dir integer,';
$p = "$p p_max integer";
test_function_exists('_util_pgck', $p, 'void');


#
# Tests for vefin._util_langck()
#
test_function_exists('_util_langck', 'p_code text', 'void');


#
# Tests for vefin._util_tzck()
#
test_function_exists('_util_tzck', 'p_tz text', 'void');


test_teardown();
