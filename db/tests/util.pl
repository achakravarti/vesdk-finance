use warnings;
use strict;
use utf8;
use open ':std', ':encoding(UTF-8)';
use DBI;
use Text::ParseWords;
use File::Basename;


# Global database handler.
our $DBH = undef;


#
# Sets up the test fixture.
#
sub test_setup
{
    my $dbname = 'vefin_test';
    my $host = 'localhost';
    my $port = 5432;
    my $username = '_vefin_test';
    my $password = '_vefin_test';

    $DBH = DBI->connect("dbi:Pg:dbname=$dbname;host=$host;port=$port",
			$username, $password, {
			    AutoCommit => 0,
			    RaiseError => 0,
			    PrintError => 0,
			    pg_enable_utf8 => 1})
	|| die $DBI::errstr;
}


#
# Tears down the test fixture.
#
sub test_teardown
{
    $DBH->disconnect();
}


#
# Loads test data from a CSV file.
#
sub load_test_data
{
    my ($file) = @_;
    my @data;

    $file = dirname(__FILE__) . '/data/' . $file;
    open(my $csv, '<', $file) || die "failed to open data file $file";

    foreach (<$csv>) {
	chomp;
	my @f = quotewords(',', 0, $_);
	push(@data, @f);
    }

    return @data;
}


#
# Utility function to test whether the vefin schema exists.
#

sub test_schema_exists
{
    my ($sch) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.schemata
    WHERE schema_name = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($sch) || die $sth->errstr;

    my $test = "Schema $sch exists";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


sub test_extension_exists
{
    my ($ext) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM pg_extension
    WHERE extname = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($ext) || die $sth->errstr;

    my $test = "Extension $ext exists";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


#
# Utility function to test whether a domain exists in the vefin schema.
#
sub test_domain_exists
{
    my ($dom) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM pg_type
    WHERE typname = ? AND typnamespace = 'vefin'::regnamespace;
    $$

    my $sth = $DBH->prepare($sql);
    my $test = "Domain vefin.$dom exists";
    $sth->execute($dom) || die $sth->errstr;

    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


#
# Tests whether a given table exists in the  vefin schema
#
sub test_table_exists
{
    my ($table) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.tables
    WHERE table_schema = 'vefin' AND table_name = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($table) || die $sth->errstr;

    my $test = "Table vefin.$table exists";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


sub test_table_column_exists
{
    my ($table, $col) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.columns
    WHERE table_schema = 'vefin'
          AND table_name = ?
          AND column_name = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($table, $col) || die $sth->errstr;

    my $test = "Column vefin.$table.$col exists";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


sub test_table_column_type
{
    my ($table, $col, $type) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.columns
    WHERE table_schema = 'vefin'
          AND table_name = ?
          AND column_name = ?
          AND data_type = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($table, $col, $type) || die $sth->errstr;

    my $test = "Column vefin.$table.$col type";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


sub test_table_column_null
{
    my ($table, $col) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.columns
    WHERE table_schema = 'vefin'
          AND table_name = ?
          AND column_name = ?
          AND is_nullable = 'YES';
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($table, $col) || die $sth->errstr;

    my $test = "Column vefin.$table.$col null";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


sub test_table_column_not_null
{
    my ($table, $col) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.columns
    WHERE table_schema = 'vefin'
          AND table_name = ?
          AND column_name = ?
          AND is_nullable = 'NO';
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($table, $col) || die $sth->errstr;

    my $test = "Column vefin.$table.$col not null";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


sub test_table_column_pk
{
    my ($table, $col) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.table_constraints AS c
    JOIN information_schema.key_column_usage AS k 
         ON c.table_name = k.table_name
            AND c.constraint_catalog = k.constraint_catalog
            AND c.constraint_schema = k.constraint_schema
            AND c.constraint_name = k.constraint_name
    WHERE c.constraint_type = 'PRIMARY KEY'
          AND k.table_schema = 'vefin'
          AND k.table_name = ?
          AND k.column_name = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($table, $col) || die $sth->errstr;

    my $test = "Column vefin.$table.$col PK";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


sub test_table_column_fk
{
    my ($table, $col, $ftable, $fcol) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.table_constraints AS tc
    JOIN information_schema.key_column_usage AS kcu
         ON tc.constraint_name = kcu.constraint_name
    JOIN information_schema.constraint_column_usage AS ccu
         ON ccu.constraint_name = tc.constraint_name
    WHERE constraint_type = 'FOREIGN KEY'
          AND tc.table_schema = 'vefin'
          AND tc.table_name = ?
          AND kcu.column_name = ?
          AND ccu.table_name = ?
          AND ccu.column_name = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($table, $col, $ftable, $fcol) || die $sth->errstr;

    my $test = "Column vefin.$table.$col FK";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


sub test_table_column_unique
{
    my ($table, $col) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.table_constraints AS c
    JOIN information_schema.key_column_usage AS k 
         ON c.table_name = k.table_name
            AND c.constraint_catalog = k.constraint_catalog
            AND c.constraint_schema = k.constraint_schema
            AND c.constraint_name = k.constraint_name
    WHERE c.constraint_type = 'UNIQUE'
          AND k.table_schema = 'vefin'
          AND k.table_name = ?
          AND k.column_name = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($table, $col) || die $sth->errstr;

    my $test = "Column vefin.$table.$col UNIQUE";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


sub test_table_column_default
{
    my ($table, $col, $def) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM information_schema.columns
    WHERE table_schema = 'vefin'
          AND table_name = ?
          AND column_name = ?
          AND column_default = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($table, $col, $def) || die $sth->errstr;

    my $test = "Column vefin.$table.$col default";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


#
# Utitlity function to test whether a PostgreSQL function with a given signature
# exists in the vefin schema.
#
sub test_function_exists
{
    my ($func, $param, $ret) = @_;

    my $sql = <<'    $$';
    SELECT 1
    FROM pg_catalog.pg_proc AS p
    LEFT JOIN pg_catalog.pg_namespace AS n ON n.oid = p.pronamespace
    WHERE n.nspname = 'vefin'
          AND p.proname = ?
          AND pg_catalog.pg_get_function_arguments(p.oid) = ?
          AND pg_catalog.pg_get_function_result(p.oid) = ?;
    $$

    my $sth = $DBH->prepare($sql);
    $sth->execute($func, $param, $ret) || die $sth->errstr;

    my $test = "Function vefin.$func($param) -> $ ret exists";
    my @res = $sth->fetchrow_array();
    scalar(@res) == 1 ? pass($test) : fail($test);

    $sth->finish();
}


1;
