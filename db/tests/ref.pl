#! /usr/bin/perl
# -*- mode: perl -*-
#
# db/tests/ref.pl -- tests for reference tables.
# This file is part of VE SDK Finance Microservice.
#
# SDPX-License-Identifier: ISC
# Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#

use 5.32.0;

use File::Basename;
use lib dirname(__FILE__);
require 'util.pl';

use Test::More tests => 514;


#
# Utility function to test the vefin.lang table schema.
#
sub lang_schema_test
{
    test_table_exists('lang');

    test_table_column_exists('lang', '_id');
    test_table_column_type('lang', '_id', 'smallint');
    test_table_column_pk('lang', '_id');

    test_table_column_exists('lang', '_code');
    test_table_column_type('lang', '_code', 'text');
    test_table_column_not_null('lang', '_code');
    test_table_column_unique('lang', '_code');

    test_table_column_exists('lang', '_en');
    test_table_column_type('lang', '_en', 'text');
    test_table_column_not_null('lang', '_en');
    test_table_column_unique('lang', '_en');
    # TODO: test_table_column_unique('lang', '_endo');

    test_table_column_exists('lang', '_endo');
    test_table_column_type('lang', '_endo', 'text');
    test_table_column_not_null('lang', '_endo');

    test_table_column_exists('lang', '_avail');
    test_table_column_type('lang', '_avail', 'boolean');
    test_table_column_not_null('lang', '_avail');
    test_table_column_default('lang', '_avail', 'false');
}


#
# Utility function to test the seed data in the vefin.lang table.
#
sub lang_seed_test
{
    my $sql = << '$$';
    SELECT 1
    FROM vefin.lang
    WHERE _id = ?
          AND _code = ?
          AND _en = ?;
$$
    my $sth = our $DBH->prepare($sql);
    my @data = load_test_data('lang.csv');

    for (my $i = 0; $i < scalar(@data) - 2; $i+=3) {
        $sth->execute($data[$i], $data[$i + 1], $data[$i + 2]);
        my @res = $sth->fetchrow_array();

        my $test = "Language $data[$i + 1] initial data";
        scalar(@res) == 1 ? pass($test) : fail($test);
    }

    $sth->finish();
}


#
# Utility function to test the vefin.curr table schema.
#
sub curr_schema_test
{
    test_table_exists('curr');

    test_table_column_exists('curr', '_id');
    test_table_column_type('curr', '_id', 'smallint');
    test_table_column_pk('curr', '_id');

    test_table_column_exists('curr', '_alpha');
    test_table_column_type('curr', '_alpha', 'text');
    test_table_column_not_null('curr', '_alpha');
    test_table_column_unique('curr', '_alpha');

    test_table_column_exists('curr', '_num');
    test_table_column_type('curr', '_num', 'text');
    test_table_column_not_null('curr', '_num');
    test_table_column_unique('curr', '_num');

    test_table_column_exists('curr', '_munit');
    test_table_column_type('curr', '_munit', 'smallint');
    test_table_column_not_null('curr', '_munit');

    test_table_column_exists('curr', '_entity');
    test_table_column_type('curr', '_entity', 'jsonb');
    test_table_column_not_null('curr', '_entity');
    test_table_column_unique('curr', '_entity');

    test_table_column_exists('curr', '_start');
    test_table_column_type('curr', '_start', 'timestamp with time zone');
    test_table_column_not_null('curr', '_start');
    test_table_column_default('curr', '_start',
        "'2022-07-01 00:00:00+00'::timestamp with time zone");

    test_table_column_exists('curr', '_stop');
    test_table_column_type('curr', '_stop', 'timestamp with time zone');
    test_table_column_null('curr', '_stop');
}


#
# Utility function to test the seed data in the vefin.curr table.
#
sub curr_seed_test
{
    my $sql = <<'$$';
    SELECT 1
    FROM vefin.curr
    WHERE _id = ?
          AND _alpha = ?
          AND _num = ?
          AND _munit = ?
          AND _entity->>'en' = ?;
$$
    my $sth = our $DBH->prepare($sql);
    my @data = load_test_data('curr.csv');

    for (my $i = 0; $i < scalar(@data) - 4; $i+=5) {
        $sth->execute($data[$i], $data[$i + 1], $data[$i + 2], $data[$i + 3],
		      $data[$i + 4]);
        my @res = $sth->fetchrow_array();

        my $test = "Currency $data[$i + 1] initial data";
        scalar @res == 1 ? pass($test) : fail($test);
    }

    $sth->finish();
}


#
# Utility function to test the vefin.acctype table schema.
#
sub acctype_schema_test
{
    test_table_exists('acctype');

    test_table_column_exists('acctype', '_id');
    test_table_column_type('acctype', '_id', 'smallint');
    test_table_column_pk('acctype', '_id');

    test_table_column_exists('acctype', '_code');
    test_table_column_type('acctype', '_code', 'text');
    test_table_column_not_null('acctype', '_code');
    test_table_column_unique('acctype', '_code');

    test_table_column_exists('acctype', '_desc');
    test_table_column_type('acctype', '_desc', 'jsonb');
    test_table_column_not_null('acctype', '_desc');
    test_table_column_unique('acctype', '_desc');
}


#
# Utility function to test the seed data in the vefin.acctype table.
#
sub acctype_seed_test
{
    my $sql = <<'$$';
    SELECT 1
    FROM vefin.acctype
    WHERE _id = ?
          AND _code = ?
          AND _desc->>'en' = ?;
$$
    my $sth = our $DBH->prepare($sql);
    my @data = load_test_data('acctype.csv');

    for (my $i = 0; $i < scalar(@data) - 2; $i += 3) {
        $sth->execute($data[$i], $data[$i + 1], $data[$i + 2]);
        my @res = $sth->fetchrow_array();

        my $test = "Account type $data[$i + 2] initial data";
        scalar @res == 1 ? pass($test) : fail($test);
    }

    $sth->finish();
}


#
# Utility function to test the vefin.lgrtype table schema.
#
sub lgrtype_schema_test
{
    test_table_exists('lgrtype');

    test_table_column_exists('lgrtype', '_id');
    test_table_column_type('lgrtype', '_id', 'smallint');
    test_table_column_pk('lgrtype', '_id');

    test_table_column_exists('lgrtype', '_code');
    test_table_column_type('lgrtype', '_code', 'text');
    test_table_column_not_null('lgrtype', '_code');
    test_table_column_unique('lgrtype', '_code');

    test_table_column_exists('lgrtype', '_desc');
    test_table_column_type('lgrtype', '_desc', 'jsonb');
    test_table_column_not_null('lgrtype', '_desc');
    test_table_column_unique('lgrtype', '_desc');
}


#
# Utility function to test the seed data in the vefin.lgrtype table.
#
sub lgrtype_seed_test
{
    my $sql = <<'$$';
    SELECT 1
    FROM vefin.lgrtype
    WHERE _id = ?
          AND _code = ?
          AND _desc->>'en' = ?;
$$
    my $sth = our $DBH->prepare($sql);
    my @data = load_test_data('lgrtype.csv');

    for (my $i = 0; $i < scalar(@data) - 2; $i += 3) {
        $sth->execute($data[$i], $data[$i + 1], $data[$i + 2]);
        my @res = $sth->fetchrow_array();

        my $test = "Ledger type $data[$i + 2] initial data";
        scalar @res == 1 ? pass($test) : fail($test);
    }

    $sth->finish();
}


#
# Utility function to test the vefin.srctype table schema.
#
sub srctype_schema_test
{
    test_table_exists('srctype');

    test_table_column_exists('srctype', '_id');
    test_table_column_type('srctype', '_id', 'smallint');
    test_table_column_pk('srctype', '_id');

    test_table_column_exists('srctype', '_code');
    test_table_column_type('srctype', '_code', 'text');
    test_table_column_not_null('srctype', '_code');
    test_table_column_unique('srctype', '_code');

    test_table_column_exists('srctype', '_desc');
    test_table_column_type('srctype', '_desc', 'jsonb');
    test_table_column_not_null('srctype', '_desc');
    test_table_column_unique('srctype', '_desc');
}


#
# Utility function to test the seed data in the vefin.srctype table.
#
sub srctype_seed_test
{
    my $sql = <<'$$';
    SELECT 1
    FROM vefin.srctype
    WHERE _id = ?
          AND _code = ?
          AND _desc->>'en' = ?;
$$

    my $sth = our $DBH->prepare($sql);
    my @data = load_test_data('srctype.csv');

    for (my $i = 0; $i < scalar(@data) - 2; $i += 3) {
        $sth->execute($data[$i], $data[$i + 1], $data[$i + 2]);
        my @res = $sth->fetchrow_array();

        my $test = "Source document type $data[$i + 2] initial data";
        scalar @res == 1 ? pass($test) : fail($test);
    }

    $sth->finish();
}


#
# Utility function to test the vefin.mime table schema.
#
sub mime_schema_test
{
    test_table_exists('mime');

    test_table_column_exists('mime', '_id');
    test_table_column_type('mime', '_id', 'smallint');
    test_table_column_pk('mime', '_id');

    test_table_column_exists('mime', '_mime');
    test_table_column_type('mime', '_mime', 'text');
    test_table_column_not_null('mime', '_mime');
    test_table_column_unique('mime', '_mime');

    test_table_column_exists('mime', '_desc');
    test_table_column_type('mime', '_desc', 'jsonb');
    test_table_column_not_null('mime', '_desc');
    test_table_column_unique('mime', '_desc');
}


#
# Utility function to test the seed data in the vefin.mime table.
#
sub mime_seed_test
{
    my $sql = <<'$$';
    SELECT 1
    FROM vefin.mime
    WHERE _id = ?
          AND _mime = ?
          AND _desc->>'en' = ?;
$$
    my $sth = our $DBH->prepare($sql);
    my @data = load_test_data('mime.csv');

    for (my $i = 0; $i < scalar(@data) - 2; $i += 3) {
        $sth->execute($data[$i], $data[$i + 1], $data[$i + 2]);
        my @res = $sth->fetchrow_array();

        my $test = "MIME type $data[$i + 1] initial data";
        scalar @res == 1 ? pass($test) : fail($test);
    }

    $sth->finish();
}


#
# Utility function to test the vefin.pol table schema.
#
sub pol_schema_test
{
    test_table_exists('pol');

    test_table_column_exists('pol', '_id');
    test_table_column_type('pol', '_id', 'smallint');
    test_table_column_pk('pol', '_id');

    test_table_column_exists('pol', '_code');
    test_table_column_type('pol', '_code', 'text');
    test_table_column_not_null('pol', '_code');
    test_table_column_unique('pol', '_code');

    test_table_column_exists('pol', '_desc');
    test_table_column_type('pol', '_desc', 'jsonb');
    test_table_column_not_null('pol', '_desc');
    test_table_column_unique('pol', '_desc');
}


#
# Utility function to test the seed data in the vefin.pol table.
#
sub pol_seed_test
{
    my $sql = <<'$$';
    SELECT 1
    FROM vefin.pol
    WHERE _id = ?
          AND _code = ?
          AND _desc->>'en' = ?;
$$
    my $sth = our $DBH->prepare($sql);
    my @data = load_test_data('pol.csv');

    for (my $i = 0; $i < scalar(@data) - 2; $i += 3) {
        $sth->execute($data[$i], $data[$i + 1], $data[$i + 2]);
        my @res = $sth->fetchrow_array();

        my $test = "Policy $data[$i + 2] initial data";
        scalar @res == 1 ? pass($test) : fail($test);
    }

    $sth->finish();
}


#
# Utility function to test the vefin.stat table schema.
#
sub stat_schema_test
{
    test_table_exists('stat');

    test_table_column_exists('stat', '_id');
    test_table_column_type('stat', '_id', 'smallint');
    test_table_column_pk('stat', '_id');

    test_table_column_exists('stat', '_code');
    test_table_column_type('stat', '_code', 'text');
    test_table_column_not_null('stat', '_code');
    test_table_column_unique('stat', '_code');

    test_table_column_exists('stat', '_desc');
    test_table_column_type('stat', '_desc', 'jsonb');
    test_table_column_not_null('stat', '_desc');
    test_table_column_unique('stat', '_desc');
}


#
# Utility function to test the seed data in the vefin.stat table.
#
sub stat_seed_test
{
    my $sql = <<'$$';
    SELECT 1
    FROM vefin.stat
    WHERE _id = ?
          AND _code = ?
          AND _desc->>'en' = ?;
$$
    my $sth = our $DBH->prepare($sql);
    my @data = load_test_data('stat.csv');

    for (my $i = 0; $i < scalar(@data) - 2; $i += 3) {
        $sth->execute($data[$i], $data[$i + 1], $data[$i + 2]);
        my @res = $sth->fetchrow_array();

        my $test = "Status $data[$i + 2] initial data";
        scalar @res == 1 ? pass($test) : fail($test);
    }

    $sth->finish();
}


# Setup
test_setup();

lang_schema_test();
lang_seed_test();
curr_schema_test();
curr_seed_test();
acctype_schema_test();
acctype_seed_test();
lgrtype_schema_test();
lgrtype_seed_test();
srctype_schema_test();
srctype_seed_test();
mime_schema_test();
mime_seed_test();
pol_schema_test();
pol_seed_test();
stat_schema_test();
stat_seed_test();


# Teardown
test_teardown();
