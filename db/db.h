#ifndef VEFIN_DB_H
#define VEFIN_DB_H

#include <libpq-fe.h>

#include "../util/util.h"


/*
 * PostgreSQL interface
 */

struct pg {
	PGconn *conn;
	PGresult *res;
};

__BEGIN_DECLS
extern void pg_new(struct pg *);
extern void pg_free(struct pg *);
extern const char *pg_exec(struct pg *, const char *, const char *[]);
__END_DECLS


#endif /* !VEFIN_DB_H */
