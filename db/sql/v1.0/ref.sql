-- ISO 639-1 language codes
-- TODO: _endo should be NOT NULL UNIQUE
CREATE TABLE IF NOT EXISTS vefin.lang (
       _id     smallint GENERATED ALWAYS AS IDENTITY,
       _code   text NOT NULL UNIQUE CHECK (_code ~ '[[:alpha:]]{2}$'),
       _en     text NOT NULL UNIQUE,
       _endo   text NOT NULL DEFAULT '',
       _avail  boolean NOT NULL DEFAULT FALSE,
       PRIMARY KEY (_id)
);

CREATE INDEX idx_gin_vefin_lang_en
ON vefin.lang USING gin (_en vefin.gin_trgm_ops);

CREATE INDEX idx_gin_vefin_lang_endo
ON vefin.lang USING gin (_endo vefin.gin_trgm_ops);

CREATE INDEX idx_vefin_lang_avail
ON vefin.lang (_avail);


--
-- ISO 4217 currency codes.
-- July 01 2022 is taken as start date, when SLL Sierra Leone leone was replaced
-- by SLE. Excludes XTS and XXX codes (testing and no currency).
--
CREATE TABLE IF NOT EXISTS vefin.curr (
    _id     smallint GENERATED ALWAYS AS IDENTITY,
    _alpha  text NOT NULL UNIQUE CHECK (_alpha ~ '[[:alpha:]]{3}$'),
    _num    text NOT NULL UNIQUE CHECK (_num ~ '\d\d\d$'),
    _munit  smallint NOT NULL CHECK (_munit > -1),
    _entity jsonb NOT NULL UNIQUE,
    _start  timestamp WITH TIME ZONE NOT NULL DEFAULT '2022-07-01 00:00:00',
    _stop   timestamp WITH TIME ZONE,
    PRIMARY KEY (_id)
);


-- Types of accounts
CREATE TABLE IF NOT EXISTS vefin.acctype (
       _id     smallint GENERATED ALWAYS AS IDENTITY,
       _code   text NOT NULL UNIQUE CHECK (_code ~ '[[:alpha:]]{1}$'),
       _desc   jsonb NOT NULL UNIQUE,
       PRIMARY KEY (_id)
);


-- Ledger types
CREATE TABLE IF NOT EXISTS vefin.lgrtype (
       _id     smallint GENERATED ALWAYS AS IDENTITY,
       _code   text NOT NULL UNIQUE CHECK (_code ~ '[[:alpha:]]{1}$'),
       _desc   jsonb NOT NULL UNIQUE,
       PRIMARY KEY (_id)
);


-- Source document types
CREATE TABLE IF NOT EXISTS vefin.srctype (
       _id     smallint GENERATED ALWAYS AS IDENTITY,
       _code   text NOT NULL UNIQUE CHECK (_code ~ '[[:alpha:]]{2}$'),
       _desc   jsonb NOT NULL UNIQUE,
       PRIMARY KEY (_id)
);


-- Common MIME types for source documents
CREATE TABLE IF NOT EXISTS vefin.mime (
       _id     smallint GENERATED ALWAYS AS IDENTITY,
       _mime   text NOT NULL UNIQUE,
       _desc   jsonb NOT NULL UNIQUE,
       PRIMARY KEY (_id)
);


-- Posting approval policy at next higher level of hierarchy
CREATE TABLE IF NOT EXISTS vefin.pol (
       _id     smallint GENERATED ALWAYS AS IDENTITY,
       _code   text NOT NULL UNIQUE CHECK (_code ~ '[[:alpha:]]{1}$'),
       _desc   jsonb NOT NULL UNIQUE,
       PRIMARY KEY (_id)
);


-- Status of journal entries
CREATE TABLE IF NOT EXISTS vefin.stat(
       _id     smallint GENERATED ALWAYS AS IDENTITY,
       _code   text NOT NULL UNIQUE CHECK (_code ~ '[[:alpha:]]{1}$'),
       _desc   jsonb NOT NULL UNIQUE,
       PRIMARY KEY (_id)
);






CREATE OR REPLACE FUNCTION vefin._lang_filter_tot (
    p_flt  text
) RETURNS bigint
LANGUAGE 'sql' STABLE AS
$$
    SELECT count(_id)
    FROM vefin.lang
    WHERE (_code ILIKE p_flt OR _en ILIKE p_flt OR _endo ILIKE p_flt)
          AND _avail = TRUE
$$;


CREATE OR REPLACE FUNCTION vefin.lang_filter (
    p_flt  vefin.filter,
    p_page vefin.page DEFAULT '1,10'
) RETURNS TABLE (
    id    vefin.lang._id % TYPE,
    code  vefin.lang._code % TYPE,
    en    vefin.lang._en % TYPE,
    endo  vefin.lang._endo % TYPE,
    len   bigint
) LANGUAGE 'plpgsql' STABLE AS
$$
DECLARE
    flt  text;
    page bigint[];
    tot  bigint;

BEGIN
    flt := (SELECT vefin.filter_parse(p_flt));
    page := (SELECT vefin.page_parse(p_page));
    tot := (SELECT vefin._lang_filter_tot(flt));

    RETURN QUERY
    SELECT _id, _code, _en, _endo, tot
    FROM vefin.lang
    WHERE (_code ILIKE flt OR _en ILIKE flt OR _endo ILIKE flt)
          AND _avail = TRUE
    ORDER BY _en, _code
    LIMIT (page[2]) OFFSET (page[1]);
END;
$$;


CREATE OR REPLACE FUNCTION vefin.lang_filter_json (
    p_flt  vefin.filter,
    p_page vefin.page DEFAULT '1,10'
) RETURNS json
LANGUAGE 'plpgsql' STABLE AS
$$
DECLARE
    flt  text;
    page bigint[];
    tot  bigint;
    ret  json;

BEGIN
    flt := (SELECT vefin.filter_parse(p_flt));
    page := (SELECT vefin.page_parse(p_page));
    tot := (SELECT vefin._lang_filter_tot(flt));

    ret := (
        SELECT json_build_object('lang', json_agg(row_to_json(r)), 'tot', tot)
        FROM (
            SELECT
	        _id AS id,
		_code AS code,
		_en AS en,
		_endo AS endo
    	    FROM vefin.lang
    	    WHERE (_code ILIKE flt OR _en ILIKE flt OR _endo ILIKE flt)
              	  AND _avail = TRUE
    	    ORDER BY _en, _code
    	    LIMIT (page[2]) OFFSET (page[1])
    	) AS r
    );

    RETURN ret;
END;
$$;




--
-- Lists the records in the currency table according to a customised paginated
-- layout.
--
CREATE OR REPLACE FUNCTION vefin.curr_list (
    p_ini  integer DEFAULT 1,
    p_len  integer DEFAULT 5,
    p_col  integer DEFAULT 3,
    p_dir  integer DEFAULT 0,
    p_lang char (2) DEFAULT 'en',
    p_tz   text DEFAULT 'utc'
) RETURNS TABLE (
    id     vefin.curr._id % TYPE,
    alpha  vefin.curr._alpha % TYPE,
    num    vefin.curr._num % TYPE,
    munit  vefin.curr._munit % TYPE,
    entity text,
    init   timestamp,
    fini   timestamp
) LANGUAGE 'plpgsql' STABLE AS
$$
DECLARE
    cols text [];
    dirs text [];

BEGIN
    PERFORM vefin._util_pgck(p_ini, p_len, p_col, p_dir, 7);
    PERFORM vefin._util_langck(p_lang);
    PERFORM vefin._util_tzck(p_tz);

    cols := (
        SELECT ARRAY [
	    '_id',
	    '_alpha',
	    '_num',
	    '_munit',
	    '_entity',
	    '_start',
	    '_stop'
	]
    );

    dirs := (SELECT ARRAY ['DESC', 'ASC', 'ASC']);

    RETURN QUERY
    EXECUTE format('
        SELECT
            _id,
            _alpha,
            _num,
            _munit,
            _entity->>''%s'',
	    _start AT TIME ZONE ''%s'',
            _stop AT TIME ZONE ''%s''
        FROM vefin.curr
        ORDER BY %s %s
        LIMIT %s OFFSET %s',
	p_lang, p_tz, p_tz, cols [p_col], dirs [p_dir + 2], p_len, p_ini - 1
    );
END;
$$;

--
-- ISO 639-1 language codes
--

INSERT INTO vefin.lang (_code, _en)
VALUES
    ('ab', 'Abkhazian'),
    ('aa', 'Afar'),
    ('af', 'Afrikaans'),
    ('ak', 'Akan'),
    ('sq', 'Albanian'),
    ('am', 'Amharic'),
    ('ar', 'Arabic'),
    ('an', 'Aragonese'),
    ('hy', 'Armenian'),
    ('as', 'Assamese'),
    ('av', 'Avaric'),
    ('ae', 'Avestan'),
    ('ay', 'Aymara'),
    ('az', 'Azerbaijani'),
    ('bm', 'Bambara'),
    ('ba', 'Bashkir'),
    ('eu', 'Basque'),
    ('be', 'Belarusian'),
    ('bn', 'Bengali'),
    ('bi', 'Bislama'),
    ('bs', 'Bosnian'),
    ('br', 'Breton'),
    ('bg', 'Bulgarian'),
    ('my', 'Burmese'),
    ('ca', 'Catalan'),
    ('km', 'Central Khmer'),
    ('ch', 'Chamorro'),
    ('ce', 'Chechen'),
    ('ny', 'Chichewa'),
    ('zh', 'Chinese'),
    ('cu', 'Church Slavonic'),
    ('cv', 'Chuvash'),
    ('kw', 'Cornish'),
    ('co', 'Corsican'),
    ('cr', 'Cree'),
    ('hr', 'Croatian'),
    ('cs', 'Czech'),
    ('da', 'Danish'),
    ('dv', 'Divehi'),
    ('nl', 'Dutch'),
    ('dz', 'Dzongkha'),
    ('en', 'English'),
    ('eo', 'Esperanto'),
    ('et', 'Estonian'),
    ('ee', 'Ewe'),
    ('fo', 'Faroese'),
    ('fj', 'Fijian'),
    ('fi', 'Finnish'),
    ('fr', 'French'),
    ('ff', 'Fulah'),
    ('gd', 'Gaelic'),
    ('gl', 'Galician'),
    ('lg', 'Ganda'),
    ('ka', 'Georgian'),
    ('de', 'German'),
    ('el', 'Greek'),
    ('gn', 'Guarani'),
    ('gu', 'Gujarati'),
    ('ht', 'Haitian'),
    ('ha', 'Hausa'),
    ('he', 'Hebrew'),
    ('hz', 'Herero'),
    ('hi', 'Hindi'),
    ('ho', 'Hiri Motu'),
    ('hu', 'Hungarian'),
    ('is', 'Icelandic'),
    ('io', 'Ido'),
    ('ig', 'Igbo'),
    ('id', 'Indonesian'),
    ('ia', 'Interlingua'),
    ('ie', 'Interlingue'),
    ('iu', 'Inuktitut'),
    ('ik', 'Inupiaq'),
    ('ga', 'Irish'),
    ('it', 'Italian'),
    ('ja', 'Japanese'),
    ('jv', 'Javanese'),
    ('kl', 'Kalaallisut'),
    ('kn', 'Kannada'),
    ('kr', 'Kanuri'),
    ('ks', 'Kashmiri'),
    ('kk', 'Kazakh'),
    ('ki', 'Kikuyu'),
    ('rw', 'Kinyarwanda'),
    ('ky', 'Kirghiz'),
    ('kv', 'Komi'),
    ('kg', 'Kongo'),
    ('ko', 'Korean'),
    ('kj', 'Kuanyama'),
    ('ku', 'Kurdish'),
    ('lo', 'Lao'),
    ('la', 'Latin'),
    ('lv', 'Latvian'),
    ('li', 'Limburgan'),
    ('ln', 'Lingala'),
    ('lt', 'Lithuanian'),
    ('lu', 'Luba-Katanga'),
    ('lb', 'Luxembourgish'),
    ('mk', 'Macedonian'),
    ('mg', 'Malagasy'),
    ('ms', 'Malay'),
    ('ml', 'Malayalam'),
    ('mt', 'Maltese'),
    ('gv', 'Manx'),
    ('mi', 'Maori'),
    ('mr', 'Marathi'),
    ('mh', 'Marshallese'),
    ('mn', 'Mongolian'),
    ('na', 'Nauru'),
    ('nv', 'Navajo'),
    ('ng', 'Ndonga'),
    ('ne', 'Nepali'),
    ('nd', 'North Ndebele'),
    ('se', 'Northern Sami'),
    ('no', 'Norwegian'),
    ('nb', 'Norwegian Bokmål'),
    ('nn', 'Norwegian Nynorsk'),
    ('oc', 'Occitan'),
    ('oj', 'Ojibwa'),
    ('or', 'Oriya'),
    ('om', 'Oromo'),
    ('os', 'Ossetian'),
    ('pi', 'Pali'),
    ('ps', 'Pashto'),
    ('fa', 'Persian'),
    ('pl', 'Polish'),
    ('pt', 'Portuguese'),
    ('pa', 'Punjabi'),
    ('qu', 'Quechua'),
    ('ro', 'Romanian'),
    ('rm', 'Romansh'),
    ('rn', 'Rundi'),
    ('ru', 'Russian'),
    ('sm', 'Samoan'),
    ('sg', 'Sango'),
    ('sa', 'Sanskrit'),
    ('sc', 'Sardinian'),
    ('sr', 'Serbian'),
    ('sn', 'Shona'),
    ('ii', 'Sichuan Yi'),
    ('sd', 'Sindhi'),
    ('si', 'Sinhala'),
    ('sk', 'Slovak'),
    ('sl', 'Slovenian'),
    ('so', 'Somali'),
    ('nr', 'South Ndebele'),
    ('st', 'Southern Sotho'),
    ('es', 'Spanish'),
    ('su', 'Sundanese'),
    ('sw', 'Swahili'),
    ('ss', 'Swati'),
    ('sv', 'Swedish'),
    ('tl', 'Tagalog'),
    ('ty', 'Tahitian'),
    ('tg', 'Tajik'),
    ('ta', 'Tamil'),
    ('tt', 'Tatar'),
    ('te', 'Telugu'),
    ('th', 'Thai'),
    ('bo', 'Tibetan'),
    ('ti', 'Tigrinya'),
    ('to', 'Tonga'),
    ('ts', 'Tsonga'),
    ('tn', 'Tswana'),
    ('tr', 'Turkish'),
    ('tk', 'Turkmen'),
    ('tw', 'Twi'),
    ('ug', 'Uighur'),
    ('uk', 'Ukrainian'),
    ('ur', 'Urdu'),
    ('uz', 'Uzbek'),
    ('ve', 'Venda'),
    ('vi', 'Vietnamese'),
    ('vo', 'Volapük'),
    ('wa', 'Walloon'),
    ('cy', 'Welsh'),
    ('fy', 'Western Frisian'),
    ('wo', 'Wolof'),
    ('xh', 'Xhosa'),
    ('yi', 'Yiddish'),
    ('yo', 'Yoruba'),
    ('za', 'Zhuang'),
    ('zu', 'Zulu')
ON CONFLICT DO NOTHING;

UPDATE vefin.lang
SET _endo = 'English', _avail = TRUE
WHERE _code = 'en';

UPDATE vefin.lang
SET _endo = 'বাংলা', _avail = TRUE
WHERE _code = 'bn';


--
-- ISO 4217 currency codes.
-- July 01 2022 is taken as start date, when SLL Sierra Leone leone was replaced
-- by SLE. Excludes XTS and XXX codes (testing and no currency).
--

INSERT INTO vefin.curr (_alpha, _num, _munit, _entity)
VALUES
    ('AED', '784', 2, '{"en": "United Arab Emirates dirham"}'),
    ('AFN', '971', 2, '{"en": "Afghan afghani"}'),
    ('ALL', '008', 2, '{"en": "Albanian lek"}'),
    ('AMD', '051', 2, '{"en": "Armenian dram"}'),
    ('ANG', '532', 2, '{"en": "Netherlands Antillean guilder"}'),
    ('AOA', '973', 2, '{"en": "Angolan kwanza"}'),
    ('ARS', '032', 2, '{"en": "Argentine peso"}'),
    ('AUD', '036', 2, '{"en": "Australian dollar"}'),
    ('AWG', '533', 2, '{"en": "Aruban florin"}'),
    ('AZN', '944', 2, '{"en": "Azerbaijani manat"}'),
    ('BAM', '977', 2, '{"en": "Bosnia and Herzegovina convertible mark"}'),
    ('BBD', '052', 2, '{"en": "Barbados dollar"}'),
    ('BDT', '050', 2, '{"en": "Bangladeshi taka"}'),
    ('BGN', '975', 2, '{"en": "Bulgarian lev"}'),
    ('BHD', '048', 3, '{"en": "Bahraini dinar"}'),
    ('BIF', '108', 0, '{"en": "Burundian franc"}'),
    ('BMD', '060', 2, '{"en": "Bermudian dollar"}'),
    ('BND', '096', 2, '{"en": "Brunei dollar"}'),
    ('BOB', '068', 2, '{"en": "Boliviano"}'),
    ('BOV', '984', 2, '{"en": "Bolivian Mvdol"}'),
    ('BRL', '986', 2, '{"en": "Brazilian real"}'),
    ('BSD', '044', 2, '{"en": "Bahamian dollar"}'),
    ('BTN', '064', 2, '{"en": "Bhutanese ngultrum"}'),
    ('BWP', '072', 2, '{"en": "Botswana pula"}'),
    ('BYN', '933', 2, '{"en": "Belarusian ruble"}'),
    ('BZD', '084', 2, '{"en": "Belize dollar"}'),
    ('CAD', '124', 2, '{"en": "Canadian dollar"}'),
    ('CDF', '976', 2, '{"en": "Congolese franc"}'),
    ('CHE', '947', 2, '{"en": "WIR euro"}'),
    ('CHF', '756', 2, '{"en": "Swiss franc"}'),
    ('CHW', '948', 2, '{"en": "WIR franc"}'),
    ('CLF', '990', 4, '{"en": "Unidad de Fomento"}'),
    ('CLP', '152', 0, '{"en": "Chilean peso"}'),
    ('COP', '170', 2, '{"en": "Colombian peso"}'),
    ('COU', '970', 2, '{"en": "Unidad de Valor Real"}'),
    ('CRC', '188', 2, '{"en": "Costa Rican colon"}'),
    ('CUC', '931', 2, '{"en": "Cuban convertible peso"}'),
    ('CUP', '192', 2, '{"en": "Cuban peso"}'),
    ('CVE', '132', 2, '{"en": "Cape Verdean escudo"}'),
    ('CZK', '203', 2, '{"en": "Czech koruna"}'),
    ('DJF', '262', 0, '{"en": "Djiboutian franc"}'),
    ('DKK', '208', 2, '{"en": "Danish krone"}'),
    ('DOP', '214', 2, '{"en": "Dominican peso"}'),
    ('DZD', '012', 2, '{"en": "Algerian dinar"}'),
    ('EGP', '818', 2, '{"en": "Egyptian pound"}'),
    ('ERN', '232', 2, '{"en": "Eritrean nakfa"}'),
    ('ETB', '230', 2, '{"en": "Ethiopian birr"}'),
    ('EUR', '978', 2, '{"en": "Euro"}'),
    ('FJD', '242', 2, '{"en": "Fiji dollar"}'),
    ('FKP', '238', 2, '{"en": "Falkland Islands pound"}'),
    ('GBP', '826', 2, '{"en": "Pound sterling"}'),
    ('GEL', '981', 2, '{"en": "Georgian lari"}'),
    ('GHS', '936', 2, '{"en": "Ghanaian cedi"}'),
    ('GIP', '292', 2, '{"en": "Gibraltar pound"}'),
    ('GMD', '270', 2, '{"en": "Gambian dalasi"}'),
    ('GNF', '324', 0, '{"en": "Guinean franc"}'),
    ('GTQ', '320', 2, '{"en": "Guatemalan quetzal"}'),
    ('GYD', '328', 2, '{"en": "Guyanese dollar"}'),
    ('HKD', '344', 2, '{"en": "Hong Kong dollar"}'),
    ('HNL', '340', 2, '{"en": "Honduran lempira"}'),
    ('HRK', '191', 2, '{"en": "Croatian kuna"}'),
    ('HTG', '332', 2, '{"en": "Haitian gourde"}'),
    ('HUF', '348', 2, '{"en": "Hungarian forint"}'),
    ('IDR', '360', 2, '{"en": "Indonesian rupiah"}'),
    ('ILS', '376', 2, '{"en": "Israeli new shekel"}'),
    ('INR', '356', 2, '{"en": "Indian rupee"}'),
    ('IQD', '368', 3, '{"en": "Iraqi dinar"}'),
    ('IRR', '364', 2, '{"en": "Iranian rial"}'),
    ('ISK', '352', 0, '{"en": "Icelandic króna"}'),
    ('JMD', '388', 2, '{"en": "Jamaican dollar"}'),
    ('JOD', '400', 3, '{"en": "Jordanian dinar"}'),
    ('JPY', '392', 0, '{"en": "Japanese yen"}'),
    ('KES', '404', 2, '{"en": "Kenyan shilling"}'),
    ('KGS', '417', 2, '{"en": "Kyrgyzstani som"}'),
    ('KHR', '116', 2, '{"en": "Cambodian riel"}'),
    ('KMF', '174', 0, '{"en": "Comoro franc"}'),
    ('KPW', '408', 2, '{"en": "North Korean won"}'),
    ('KRW', '410', 0, '{"en": "South Korean won"}'),
    ('KWD', '414', 3, '{"en": "Kuwaiti dinar"}'),
    ('KYD', '136', 2, '{"en": "Cayman Islands dollar"}'),
    ('KZT', '398', 2, '{"en": "Kazakhstani tenge"}'),
    ('LAK', '418', 2, '{"en": "Lao kip"}'),
    ('LBP', '422', 2, '{"en": "Lebanese pound"}'),
    ('LKR', '144', 2, '{"en": "Sri Lankan rupee"}'),
    ('LRD', '430', 2, '{"en": "Liberian dollar"}'),
    ('LSL', '426', 2, '{"en": "Lesotho loti"}'),
    ('LYD', '434', 3, '{"en": "Libyan dinar"}'),
    ('MAD', '504', 2, '{"en": "Moroccan dirham"}'),
    ('MDL', '498', 2, '{"en": "Moldovan leu"}'),
    ('MGA', '969', 2, '{"en": "Malagasy ariary"}'),
    ('MKD', '807', 2, '{"en": "North Macedonian denar"}'),
    ('MMK', '104', 2, '{"en": "Myanmar kyat"}'),
    ('MNT', '496', 2, '{"en": "Mongolian tögrög"}'),
    ('MOP', '446', 2, '{"en": "Macanese pataca"}'),
    ('MRU', '929', 2, '{"en": "Mauritanian ouguiya"}'),
    ('MUR', '480', 2, '{"en": "Mauritian rupee"}'),
    ('MVR', '462', 2, '{"en": "Maldivian rufiyaa"}'),
    ('MWK', '454', 2, '{"en": "Malawian kwacha"}'),
    ('MXN', '484', 2, '{"en": "Mexican peso"}'),
    ('MXV', '979', 2, '{"en": "Mexican Unidad de Inversion"}'),
    ('MYR', '458', 2, '{"en": "Malaysian ringgit"}'),
    ('MZN', '943', 2, '{"en": "Mozambican metical"}'),
    ('NAD', '516', 2, '{"en": "Namibian dollar"}'),
    ('NGN', '566', 2, '{"en": "Nigerian naira"}'),
    ('NIO', '558', 2, '{"en": "Nicaraguan córdoba"}'),
    ('NOK', '578', 2, '{"en": "Norwegian krone"}'),
    ('NPR', '524', 2, '{"en": "Nepalese rupee"}'),
    ('NZD', '554', 2, '{"en": "New Zealand dollar"}'),
    ('OMR', '512', 3, '{"en": "Omani rial"}'),
    ('PAB', '590', 2, '{"en": "Panamanian balboa"}'),
    ('PEN', '604', 2, '{"en": "Peruvian sol"}'),
    ('PGK', '598', 2, '{"en": "Papua New Guinean kina"}'),
    ('PHP', '608', 2, '{"en": "Philippine peso"}'),
    ('PKR', '586', 2, '{"en": "Pakistani rupee"}'),
    ('PLN', '985', 2, '{"en": "Polish złoty"}'),
    ('PYG', '600', 0, '{"en": "Paraguayan guaraní"}'),
    ('QAR', '634', 2, '{"en": "Qatari riyal"}'),
    ('RON', '946', 2, '{"en": "Romanian leu"}'),
    ('RSD', '941', 2, '{"en": "Serbian dinar"}'),
    ('CNY', '156', 2, '{"en": "Renminbi"}'),
    ('RUB', '643', 2, '{"en": "Russian ruble"}'),
    ('RWF', '646', 0, '{"en": "Rwandan franc"}'),
    ('SAR', '682', 2, '{"en": "Saudi riyal"}'),
    ('SBD', '090', 2, '{"en": "Solomon Islands dollar"}'),
    ('SCR', '690', 2, '{"en": "Seychelles rupee"}'),
    ('SDG', '938', 2, '{"en": "Sudanese pound"}'),
    ('SEK', '752', 2, '{"en": "Swedish krona"}'),
    ('SGD', '702', 2, '{"en": "Singapore dollar"}'),
    ('SHP', '654', 2, '{"en": "Saint Helena pound"}'),
    ('SLE', '694', 2, '{"en": "Sierra Leonean leone"}'),
    ('SOS', '706', 2, '{"en": "Somali shilling"}'),
    ('SRD', '968', 2, '{"en": "Surinamese dollar"}'),
    ('SSP', '728', 2, '{"en": "South Sudanese pound"}'),
    ('STN', '930', 2, '{"en": "São Tomé and Príncipe dobra"}'),
    ('SVC', '222', 2, '{"en": "Salvadoran colón"}'),
    ('SYP', '760', 2, '{"en": "Syrian pound"}'),
    ('SZL', '748', 2, '{"en": "Swazi lilangeni"}'),
    ('THB', '764', 2, '{"en": "Thai baht"}'),
    ('TJS', '972', 2, '{"en": "Tajikistani somoni"}'),
    ('TMT', '934', 2, '{"en": "Turkmenistan manat"}'),
    ('TND', '788', 3, '{"en": "Tunisian dinar"}'),
    ('TOP', '776', 2, '{"en": "Tongan paʻanga"}'),
    ('TRY', '949', 2, '{"en": "Turkish lira"}'),
    ('TTD', '780', 2, '{"en": "Trinidad and Tobago dollar"}'),
    ('TWD', '901', 2, '{"en": "New Taiwan dollar"}'),
    ('TZS', '834', 2, '{"en": "Tanzanian shilling"}'),
    ('UAH', '980', 2, '{"en": "Ukrainian hryvnia"}'),
    ('UGX', '800', 0, '{"en": "Ugandan shilling"}'),
    ('USD', '840', 2, '{"en": "United States dollar"}'),
    ('USN', '997', 2, '{"en": "United States dollar (next day)"}'),
    ('UYI', '940', 0, '{"en": "Uruguay Peso en Unidades Indexadas"}'),
    ('UYU', '858', 2, '{"en": "Uruguayan peso"}'),
    ('UYW', '927', 4, '{"en": "Unidad previsional"}'),
    ('UZS', '860', 2, '{"en": "Uzbekistan sum"}'),
    ('VED', '926', 2, '{"en": "Venezuelan digital bolívar"}'),
    ('VES', '928', 2, '{"en": "Venezuelan sovereign bolívar"}'),
    ('VND', '704', 0, '{"en": "Vietnamese đồng"}'),
    ('VUV', '548', 0, '{"en": "Vanuatu vatu"}'),
    ('WST', '882', 2, '{"en": "Samoan tala"}'),
    ('XAF', '950', 0, '{"en": "CFA franc BEAC"}'),
    ('XAG', '961', 0, '{"en": "Silver"}'),
    ('XAU', '959', 0, '{"en": "Gold"}'),
    ('XBA', '955', 0, '{"en": "European Composite Unit"}'),
    ('XBB', '956', 0, '{"en": "European Monetary Unit"}'),
    ('XBC', '957', 0, '{"en": "European Unit of Account 9"}'),
    ('XBD', '958', 0, '{"en": "European Unit of Account 17"}'),
    ('XCD', '951', 2, '{"en": "East Caribbean dollar"}'),
    ('XDR', '960', 0, '{"en": "Special drawing rights"}'),
    ('XOF', '952', 0, '{"en": "CFA franc BCEAO"}'),
    ('XPD', '964', 0, '{"en": "Palladium"}'),
    ('XPF', '953', 0, '{"en": "CFP franc"}'),
    ('XPT', '962', 0, '{"en": "Platinum"}'),
    ('XSU', '994', 0, '{"en": "SUCRE"}'),
    ('XUA', '965', 0, '{"en": "ADB Unit of Account"}'),
    ('YER', '886', 2, '{"en": "Yemeni rial"}'),
    ('ZAR', '710', 2, '{"en": "South African rand"}'),
    ('ZMW', '967', 2, '{"en": "Zambian kwacha"}'),
    ('ZWL', '932', 2, '{"en": "Zimbabwean dollar"}')
ON CONFLICT DO NOTHING;


--
-- Types of accounts
--

INSERT INTO vefin.acctype (_code, _desc)
VALUES
    ('A', '{"en": "Asset"}'),
    ('L', '{"en": "Liability"}'),
    ('X', '{"en": "Expense"}'),
    ('R', '{"en": "Revenue"}'),
    ('E', '{"en": "Equity"}')
ON CONFLICT DO NOTHING;


--
-- Ledger types
--

INSERT INTO vefin.lgrtype (_code, _desc)
VALUES
    ('G', '{"en": "General"}'),
    ('P', '{"en": "Purchase"}'),
    ('S', '{"en": "Sales"}')
ON CONFLICT DO NOTHING;


--
-- Source document types
--

INSERT INTO vefin.srctype (_code, _desc)
VALUES
    ('QT', '{"en": "Quote"}'),
    ('PO', '{"en": "Purchase order"}'),
    ('DD', '{"en": "Delivery docket"}'),
    ('PI', '{"en": "Purchase invoice"}'),
    ('SI', '{"en": "Sales invoice"}'),
    ('DN', '{"en": "Debit note"}'),
    ('CN', '{"en": "Credit note"}'),
    ('CQ', '{"en": "Cheque"}'),
    ('PA', '{"en": "Payment advice"}'),
    ('RC', '{"en": "Receipt"}'),
    ('VC', '{"en": "Voucher"}'),
    ('OT', '{"en": "Other"}')
ON CONFLICT DO NOTHING;


--
-- Common MIME types for source documents
--

INSERT INTO vefin.mime (_mime, _desc)
VALUES
    ('text/plain', '{"en": "Text document"}'),
    ('application/pdf', '{"en": "PDF document"}'),
    ('image/jpeg', '{"en": "JPEG image"}'),
    ('image/gif', '{"en": "GIF image"}'),
    ('image/png', '{"en": "PNG image"}'),
    ('video/avi', '{"en": "AVI video"}'),
    ('video/mp4', '{"en": "MP4 video"}'),
    ('video/mpeg', '{"en": "MPEG video"}'),
    ('video/webm', '{"en": "WebM video"}')
ON CONFLICT DO NOTHING;


--
-- Posting approval policy at next higher level of hierarchy
--

INSERT INTO vefin.pol (_code, _desc)
VALUES
    ('E', '{"en": "Either"}'),
    ('A', '{"en": "All"}')
ON CONFLICT DO NOTHING;


--
-- Status of journal entries
--

INSERT INTO vefin.stat (_code, _desc)
VALUES
    ('P', '{"en": "Pending"}'),
    ('A', '{"en": "Approved"}'),
    ('R', '{"en": "Rejected"}'),
    ('C', '{"en": "Conflict"}'),
    ('U', '{"en": "Under review"}')
ON CONFLICT DO NOTHING;
