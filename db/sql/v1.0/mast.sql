
--
-- MASTER TABLES
--


-- Business organisations for which financial records are being kept
CREATE TABLE IF NOT EXISTS vefin.org (
    _id     bigint GENERATED ALWAYS AS IDENTITY,
    _uuid   uuid NOT NULL DEFAULT gen_random_uuid(),
    _name   text NOT NULL,
    _at     timestamp WITH TIME ZONE NOT NULL,
    _lang   smallint NOT NULL,
    _note   text NULL,
    FOREIGN KEY (_lang) REFERENCES vefin.lang (_id) ON DELETE CASCADE
) PARTITION BY RANGE (_at);


-- Ledgers maintained by each organisation
CREATE TABLE IF NOT EXISTS vefin.lgr (
    _id     bigint GENERATED ALWAYS AS IDENTITY,
    _uuid   uuid NOT NULL DEFAULT gen_random_uuid(),
    _type   smallint NOT NULL,
    _org    bigint NOT NULL, -- fk to partitioned vefin.org (_id)
    _name   text NOT NULL,
    _lang   smallint NOT NULL,
    _drlim  numeric (8, 4) NULL,
    _crlim  numeric (8, 4) NULL,
    _at     timestamp WITH TIME ZONE NOT NULL,
    _note   text NULL,
    FOREIGN KEY (_type) REFERENCES vefin.lgrtype (_id) ON DELETE CASCADE,
    FOREIGN KEY (_lang) REFERENCES vefin.lang (_id) ON DELETE CASCADE
) PARTITION BY RANGE (_at);


-- Accounts within each ledger
CREATE TABLE IF NOT EXISTS vefin.acc (
    _id     bigint GENERATED ALWAYS AS IDENTITY,
    _uuid   uuid NOT NULL DEFAULT gen_random_uuid(),
    _type   smallint NOT NULL,
    _lgr    smallint NOT NULL, --fk to vefin.lgr (_id)
    _name   text NOT NULL,
    _lang   smallint NOT NULL,
    _crlim  numeric (8, 4) NULL,
    _drlim  numeric (8, 4) NULL,
    _at     timestamp WITH TIME ZONE,
    _note   text NULL,
    FOREIGN KEY (_type) REFERENCES vefin.acctype (_id) ON DELETE CASCADE,
    FOREIGN KEY (_lang) REFERENCES vefin.lang (_id) ON DELETE CASCADE
) PARTITION BY RANGE (_at);


-- Source documents for journal entries
CREATE TABLE IF NOT EXISTS vefin.src (
    _id    bigint GENERATED ALWAYS AS IDENTITY,
    _uuid  uuid NOT NULL DEFAULT gen_random_uuid(),
    _type  smallint NOT NULL,
    _url   text NOT NULL,
    _mime  smallint NOT NULL,
    _cksum text NOT NULL,
    _at    timestamp WITH TIME ZONE NOT NULL,
    _note  text NOT NULL,
    _lang  smallint NOT NULL,
    FOREIGN KEY (_type) REFERENCES vefin.srctype (_id) ON DELETE CASCADE,
    FOREIGN KEY (_mime) REFERENCES vefin.mime (_id) ON DELETE CASCADE,
    FOREIGN KEY (_lang) REFERENCES vefin.lang (_id) ON DELETE CASCADE
) PARTITION BY RANGE (_at);


-- External human user interacting with accounts
CREATE TABLE IF NOT EXISTS vefin.usr (
    _id     bigint GENERATED ALWAYS AS IDENTITY,
    _uuid   uuid NOT NULL DEFAULT gen_random_uuid(),
    _name   text NOT NULL,
    _nick   text NOT NULL,
    _email  text NOT NULL,
    _at     timestamp WITH TIME ZONE NOT NULL,
    _drlim  numeric (8, 4) NOT NULL DEFAULT 0.00, -- default limit
    _crlim  numeric (8, 4) NOT NULL DEFAULT 0.00, -- default limit
    _note   text NULL,
    _lang   smallint NOT NULL,
    FOREIGN KEY (_lang) REFERENCES vefin.lang (_id) ON DELETE CASCADE
) PARTITION BY RANGE (_at);


-- Journal entries
CREATE TABLE IF NOT EXISTS vefin.jrn (
    _id    bigint GENERATED ALWAYS AS IDENTITY,
    _uuid  uuid NOT NULL DEFAULT gen_random_uuid(),
    _curr  smallint NOT NULL,
    _dr    integer NOT NULL,
    _cr    integer NOT NULL,
    _at   timestamp with time zone NOT NULL,
    _amt   numeric (8, 4) NOT NULL,
    _note  text NOT NULL,
    _lang  smallint NOT NULL,
    FOREIGN KEY (_curr) REFERENCES vefin.curr (_id) ON DELETE CASCADE,
    FOREIGN KEY (_lang) REFERENCES vefin.lang (_id) ON DELETE CASCADE
) PARTITION BY RANGE (_at);


-- History table for audit trails

CREATE TABLE IF NOT EXISTS vefin.hist (
    _id     bigint GENERATED ALWAYS AS IDENTITY,
    _uuid   uuid NOT NULL,
    _at     timestamp WITH TIME ZONE NOT NULL,
    _usr    bigint NOT NULL, -- fk to vefin.usr
    _ctx    jsonb NOT NULL,
    _trace  jsonb,
    _prev   jsonb,
    _curr   jsonb
) PARTITION BY RANGE (_at);

CREATE INDEX IF NOT EXISTS idx_vefin_hist__uuid
ON vefin.hist (_uuid);

CREATE INDEX IF NOT EXISTS idx_vefin_hist__ctx
ON vefin.hist (_ctx);


--
-- Organisation initial partition
--

CREATE OR REPLACE FUNCTION vefin._org_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'org_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
        _tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.org
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_id);',
	    _tbl
	);
      
        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_uuid);',
	    _tbl
	);

        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_name);',
	    _tbl
	);
    END IF;
END;
$$;

SELECT vefin._org_part();


--
-- Ledger initial partitions
--

CREATE OR REPLACE FUNCTION vefin._lgr_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'lgr_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
        _tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.lgr
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_id);',
	    _tbl
	);
      
        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_uuid);',
	    _tbl
	);
	
        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_name);',
	    _tbl
	);
    END IF;
END;
$$;


SELECT vefin._lgr_part();


--
-- Account initial partitions
--

CREATE OR REPLACE FUNCTION vefin._acc_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'acc_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
	_tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.acc
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_id);',
	    _tbl
	);
      
        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_uuid);',
	    _tbl
	);

        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_name);',
	    _tbl
	);
    END IF;
END;
$$;


SELECT vefin._acc_part();


--
-- Source document initial partitions
--

CREATE OR REPLACE FUNCTION vefin._src_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'src_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
	_tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.src
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_id);',
	    _tbl
	);
      
        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_uuid);',
	    _tbl
	);

        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_url);',
	    _tbl
	);
    END IF;
END;
$$;

SELECT vefin._src_part();


--
-- User initial partitions
--

CREATE OR REPLACE FUNCTION vefin._usr_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'usr_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
	_tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.usr
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_id);',
	    _tbl
	);
      
        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_uuid);',
	    _tbl
	);

        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_email);',
	    _tbl
	);
    END IF;
END;
$$;

SELECT vefin._usr_part();


--
-- Journal initial partitions
--

CREATE OR REPLACE FUNCTION vefin._jrn_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'jrn_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
	_tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.jrn
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_id);',
	    _tbl
	);
      
        EXECUTE format(
	    'ALTER TABLE %s
	     ADD UNIQUE (_uuid);',
	    _tbl
	);
    END IF;
END;
$$;

SELECT vefin._jrn_part();


--
-- History initial partition
--

CREATE OR REPLACE FUNCTION vefin._hist_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'hist_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
	_tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.hist
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_id);',
	    _tbl
	);
    END IF;
END;
$$;

SELECT vefin._hist_part();
