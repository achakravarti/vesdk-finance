--
-- DETAIL TABLES
--


-- Rolling balances for each account after posting from journal
CREATE TABLE IF NOT EXISTS vefin.acc_bal (
    _acc    bigint NOT NULL, -- fk to vefin.acc (_id)
    _curr   smallint NOT NULL,
    _at     timestamp WITH TIME ZONE NOT NULL,
    _bal    numeric (8, 4) NOT NULL,
    PRIMARY KEY (_acc, _curr, _at),
    FOREIGN KEY (_curr) REFERENCES vefin.curr (_id) ON DELETE CASCADE
) PARTITION BY RANGE (_at);


-- Permissions for actors across ledgers
CREATE TABLE IF NOT EXISTS vefin.lgr_perm (
    _lgr    bigint NOT NULL, -- fk to vefin.lgr (_id)
    _usr    bigint NOT NULL, -- fk to vefin.usr (_id)
    _read   boolean NOT NULL DEFAULT FALSE,
    _write  boolean NOT NULL DEFAULT FALSE,
    _post   boolean NOT NULL DEFAULT FALSE,
    _hist   boolean NOT NULL DEFAULT FALSE,
    _exp    boolean NOT NULL DEFAULT FALSE,
    _over   boolean NOT NULL DEFAULT FALSE,
    _drlim  numeric (8, 4) NOT NULL DEFAULT 0.00, -- specific limit
    _crlim  numeric (8, 4) NOT NULL DEFAULT 0.00, -- specific limit
    _at     timestamp WITH TIME ZONE NOT NULL
) PARTITION BY RANGE (_at);


-- Approval hierarchy for posting to ledgers
CREATE TABLE IF NOT EXISTS vefin.lgr_hier (
    _lgr    bigint NOT NULL, -- fk to vefin.lgr (_id)
    _usr    bigint NOT NULL, -- fk to vefin.usr (_id)
    _super  bigint NULL,  -- null if top most, fk to vefin.usr (_id)
    _pol    smallint NULL, -- policy at higher level: ALL, EITHER, NULL if topmost
    _at     timestamp WITH TIME ZONE NOT NULL,
    FOREIGN KEY (_pol) REFERENCES vefin.pol (_id) ON DELETE CASCADE
) PARTITION BY RANGE (_at);


-- Source documents for journal entries
CREATE TABLE IF NOT EXISTS vefin.jrn_src (
    _jrn    bigint NOT NULL, -- fk to vefin.jrn (_id)
    _src    bigint NOT NULL, -- fk to vefin.src (_id)
    _at     timestamp with time zone NOT NULL -- required only for partitioning
) PARTITION BY RANGE (_at);


-- Approval history for journal entries
CREATE TABLE IF NOT EXISTS vefin.jrn_stat (
    _jrn    bigint NOT NULL,
    _usr    bigint NOT NULL,
    _at     timestamp with time zone NOT NULL,
    _stat   smallint NOT NULL,
    _note   text NOT NULL,
    _lang   smallint NOT NULL,
    PRIMARY KEY (_jrn, _usr, _at, _stat),
    FOREIGN KEY (_stat) REFERENCES vefin.stat (_id) ON DELETE CASCADE,
    FOREIGN KEY (_lang) REFERENCES vefin.lang (_id) ON DELETE CASCADE
) PARTITION BY RANGE (_at);


CREATE OR REPLACE FUNCTION vefin._lgr_perm_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'lgr_perm_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
        _tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.lgr_perm
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_lgr, _usr);',
	    _tbl
	);
    END IF;
END;
$$;

CREATE OR REPLACE FUNCTION vefin._lgr_hier_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'lgr_hier_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
        _tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.lgr_hier
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_lgr, _usr, _super);',
	    _tbl
	);
    END IF;
END;
$$;

SELECT vefin._lgr_perm_part();
SELECT vefin._lgr_hier_part();



CREATE OR REPLACE FUNCTION vefin._acc_bal_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'acc_bal_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
        _tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.acc_bal
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);
    END IF;
END;
$$;

SELECT vefin._acc_bal_part();



CREATE OR REPLACE FUNCTION vefin._jrn_src_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'jrn_src_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
        _tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.jrn_src
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);

        EXECUTE format(
            'ALTER TABLE %s
             ADD PRIMARY KEY (_jrn, _src);',
	    _tbl
	);
    END IF;
END;
$$;

CREATE OR REPLACE FUNCTION vefin._jrn_stat_part (
) RETURNS void
LANGUAGE 'plpgsql' VOLATILE AS
$$
DECLARE
    _yr    text;
    _tbl   text;
    _start text;
    _end   text;

BEGIN
    _yr := (SELECT extract(YEAR FROM now())::text);
    _tbl := (SELECT 'jrn_stat_' || _yr);

    IF NOT EXISTS (
        SELECT 1
	FROM information_schema.tables
	WHERE table_schema = 'vefin'
	      AND table_name = _tbl
    ) THEN
        _tbl := (SELECT 'vefin.' || _tbl);
        _start := (SELECT _yr || '-01-01 00:00:00');
        _end := (SELECT _yr || '-12-31 23:59:59');

        EXECUTE format(
	    'CREATE TABLE IF NOT EXISTS %s
             PARTITION OF vefin.jrn_stat
             FOR VALUES FROM (''%s'') TO (''%s'');',
	    _tbl, _start, _end
	);
    END IF;
END;
$$;

SELECT vefin._jrn_src_part();
SELECT vefin._jrn_stat_part();
