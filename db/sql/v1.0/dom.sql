--
-- DOMAINS
--

DO
$$
BEGIN
    CREATE DOMAIN vefin.page AS
        text NOT NULL CHECK (VALUE ~ '^[1-9][0-9]*,[1-9][0-9]*$');

    CREATE DOMAIN vefin.rank AS
        text NOT NULL CHECK (VALUE ~ '^[1-9][0-9]*,0|-?1$');

    CREATE DOMAIN vefin.filter AS
        text NOT NULL CHECK (VALUE ~ '^[^\s]+(\s+[^\s]+)*$');

EXCEPTION
    WHEN duplicate_object
    THEN NULL;
END;
$$;


--
-- Utility function to check whether the arguments to paging parameters are
-- valid.
--
CREATE OR REPLACE FUNCTION vefin._util_pgck (
    p_ini integer,
    p_len integer,
    p_col integer,
    p_dir integer,
    p_max integer
) RETURNS void
LANGUAGE 'plpgsql' STABLE AS
$$
BEGIN
    IF (p_ini < 1) THEN
        RAISE EXCEPTION
	USING ERRCODE = '2201R',
	      MESSAGE = 'Invalid pagination offset',
	      DETAIL = 'The pagination offset must be greater than 1, but '''
	               || p_ini
		       || ''' was provided.';
    END IF;

    IF (p_len < 1) THEN
        RAISE EXCEPTION
	USING ERRCODE = '2201R',
	      MESSAGE = 'Invalid pagination length',
	      DETAIL = 'The pagination length must be at least 1, but '''
	               || p_ini
		       || ''' was provided.';
    END IF;

    IF (p_col < 1 OR p_col > p_max) THEN
        RAISE EXCEPTION
	USING ERRCODE = '2201R',
	      MESSAGE = 'Invalid pagination column index',
	      DETAIL = 'The pagination column index range is [1, '
	               || p_max
		       || '], but '''
		       || p_col
		       || ''' was provided';
    END IF;

    IF (p_dir < -1 OR p_dir > 1) THEN
        RAISE EXCEPTION
	USING ERRCODE = '2201R',
	      MESSAGE = 'Invalid pagination sort direction',
	      DETAIL = 'The pagination sort direction range is [-1, 1], but '''
	               || p_dir
		       || ''' was provided.';
    END IF;
END;
$$;


--
-- Utility function to check whether a language code currently exists and is
-- available.
--
CREATE OR REPLACE FUNCTION vefin._util_langck (
    p_code text
) RETURNS void
LANGUAGE 'plpgsql' STABLE AS
$$
DECLARE
    ck integer;
    
BEGIN
    ck := (SELECT 1 FROM vefin.lang WHERE _code = p_code AND _avail = TRUE);

    IF (ck IS NULL) THEN
        RAISE EXCEPTION
	USING ERRCODE = '2201R',
	      MESSAGE = 'Invalid language code',
	      DETAIL = 'The specified language code '''
	               || p_code
		       || ''' either unrecognised or is unavailable.';
    END IF;
END;
$$;


--
-- Utility function to check whether a time zone is valid and supported by
-- PostgreSQL.
--
CREATE OR REPLACE FUNCTION vefin._util_tzck (
    p_tz text
) RETURNS void
LANGUAGE 'plpgsql' STABLE AS
$$
DECLARE
    ck integer;

BEGIN
    ck := (SELECT 1 FROM pg_timezone_names WHERE lower(name) = lower(p_tz));

    IF (ck IS NULL) THEN
        RAISE EXCEPTION
	USING ERRCODE = '2201R',
	      MESSAGE = 'Invalid time zone',
	      DETAIL = 'The specified time zone '''
	               || p_tz
		       || ''' is unrecognised.';
    END IF;
END;
$$;


CREATE OR REPLACE FUNCTION vefin.page_parse (
    p_page vefin.page
) RETURNS bigint[]
LANGUAGE 'sql' STABLE AS
$$
    SELECT ARRAY [a[1]::bigint - 1, a[2]::bigint]
    FROM string_to_array(p_page, ',') AS a;
$$;


CREATE OR REPLACE FUNCTION vefin.filter_parse (
    p_flt vefin.filter
) RETURNS text
LANGUAGE 'sql' STABLE AS
$$
	SELECT replace(p_flt, '%', '\%') || '%';
$$;
