dnl Override default quoting
changequote(<<, >>) dnl


dnl Content of AUTHORS man page section
define(__authors__,
<<VE SDK Finance Microservice is developed and maintained by
.An Abhishek Chakravarti
.Aq Mt abhishek@taranjali.org Ns .>>)


dnl Content of BUGS man page section
define(__bugs__,
<<Please report any bugs to
.An Abhishek Chakravarti
.Aq Mt abhishek@taranjali.org
mentioning all relevant details.>>)

define(__bugs2__,
<<For other bugs, please email the details to
.An -nosplit
.An Abhishek Chakravarti
.Aq Mt abhishek@taranjali.org .>>)


dnl Content of the HISTORY man page section
define(__history__,
<<The latest version of the VE SDK Finance Microservice project is available
from the public mirror at
.Lk https://codeberg.org/achakravarti/vesdk-finance.git "CodeBerg" .
.Ss Copyright
Copyright (c) 2022
.An -nosplit
.An Abhishek Chakravarti
.Aq Mt abhishek@taranjali.org Ns .
.Bd -literal -compact
SPDX-License-Identifier: ISC.
.Ed
.Pp
VE SDK Finance Microservice is free and open source software: you are free to
use and redistribute it under the provisions of the ISC License. There is NO
WARRANTY, to the extent permitted by law. See the
.Pa /usr/local/share/doc/vesdk/finance/LICENSE
file for the full license test.>>)
