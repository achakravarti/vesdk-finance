#! /bin/sh
# -*- mode: sh -*-
#
# vesdk/finance/sbin/testdb.sh -- utility for running tests.
# This file is part of the VE SDK Finance Microservice.
# See vesdk.finance.test(8) for documentation.
#
# SDPX-License-Identifier: ISC
# Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#

#
# Source dependences
#

. /usr/local/include/vesdk/init/msg.sh


#
# Preflight checks
#

vei_msg_info 'Checking dependencies'

msg='OpenBSD 7.1 or higher host required'
[ "$(uname)" = OpenBSD ] || vei_msg_fail "$msg"
[ "$(uname -r | tr -d '.')" -gt 70 ] || vei_msg_fail "$msg"

if ! prove -V 1>/dev/null 2>&1; then
    vei_msg_fail 'prove utility not found; is it installed?'
fi

if psql -V 1>/dev/null 2>&1; then
    ver=$(psql -V | cut -d ' ' -f 3 | cut -d '.' -f 1)
    [ "$ver" -gt 13 ] || 'PostgreSQL version 14 or higher is required'

    if ! pgrep -u _postgresql -f -- -D 1>/dev/null 2>&1; then
	vei_msg_fail 'PostgreSQL not running; start it first'
    fi
else
    vei_msg_fail 'psql command required; is PostgreSQL installed?'
fi


#
# Define common variables
#


db=vefin_test
usr=_vefin_test
tmp="$PWD/tmp"


#
# Create vefin_test database with user _vefin_test
#

vei_msg_info 'Creating test database and user'
vei_msg_info 'Enter postgres password if not in ~/.pgpass when prompted'

{
    echo "DROP DATABASE IF EXISTS $db WITH (FORCE);";
    echo "DROP USER IF EXISTS $usr;";

    echo "CREATE USER $usr WITH password '$usr';";
    echo "ALTER ROLE $usr SET client_encoding TO 'utf8';";
    printf 'ALTER ROLE %s SET default_transaction_isolation TO' "$usr";
    echo " 'read committed';";
    echo "ALTER ROLE $usr SET timezone TO 'UTC';";

    echo "CREATE DATABASE $db WITH OWNER=$usr;";
    echo "GRANT ALL PRIVILEGES ON DATABASE $db TO $usr;";
} > "$tmp"


PGOPTIONS='-c client_min_messages=WARNING' \
psql                                       \
    -U postgres                            \
    -h localhost                           \
    -q                                     \
    -a                                     \
    -v ON_ERROR_STOP=1                     \
    -f "$tmp" || vei_msg_fail "Could not create $db database"


#
# Create vefin schema in vefin_test database
#

vei_msg_info 'Creating test database and user'
vei_msg_info 'Enter _vefin_test password if not in ~/.pgpass when prompted'
vei_msg_info 'The default password is _vefin_test'

echo 'CREATE SCHEMA IF NOT EXISTS vefin;' > "$tmp"
echo 'CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA vefin;' >> "$tmp"

dir=$(find "$PWD/db/sql/" -type d -name 'v*' | sort -V)
for d in $dir; do
    while read -r m; do
        echo "\i $d/$m" >> "$tmp"
    done < "$d/MANIFEST"
done

PGOPTIONS='-c client_min_messages=WARNING' \
psql                                       \
    -U "$usr"                              \
    -h localhost                           \
    -d "$db"                               \
    -q                                     \
    -a                                     \
    -v ON_ERROR_STOP=1                     \
    -f "$tmp" || vei_msg_fail 'Could not create vefin schema'

rm -f "$tmp" 1>/dev/null 2>&1


#
# Run tests
#

vei_msg_info 'Running database tests'

while read -r m; do
    echo "$PWD/db/tests/$m" >> "$tmp"
done < "$PWD/db/tests/MANIFEST"

# shellcheck disable=SC2046
prove -f $(cat tmp)

rm -f "$tmp" 1>/dev/null 2>&1
vei_msg_ok 'Testing completed'
