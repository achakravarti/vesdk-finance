#! /bin/sh
#
# uninstall.sh -- uninstallation script.
# This file is part of the VE SDK Finance Microservice.
#
# SDPX-License-Identifier: ISC
# Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#


#
# Sources the VE SDK Init message script dependency. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
source_vei()
{
        if [ -f /usr/local/include/vesdk/init/msg.sh ]; then
	        . /usr/local/include/vesdk/init/msg.sh
	else
                echo 2>&1 'FAIL: vesdk-init library not found'
       		exit 1
	fi
}


#
# Checks whether script is being run as root. See also:
#   - id(1)
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
check_root()
{
        if [ "$(id -u)" -ne 0 ]; then
	        vei_msg_fail 'Permission denied; uninstall as root'
	fi
}


#
# Removes binaries. See also:
#   - groupdel(1)
#   - userdel(1)
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
rm_bin()
{
        vei_msg_info 'Removing binaries'
        msg='Could not remove binaries'
	root=/usr/local/bin

        rm -rf $root/vefin 1>/dev/null 2>&1 || vei_msg_fail "$msg"
        rm -rf $root/vefind 1>/dev/null 2>&1 || vei_msg_fail "$msg"

	vei_msg_info 'Removing _vefin user/group'
        msg='Could not remove _vefin user/group'

	userdel _vefin || vei_msg_warn "$msg"
	groupdel _vefin || vei_msg_warn "$msg"
}


#
# Removes man pages. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#   - makewhatis(8)
#
rm_man()
{
        vei_msg_info 'Removing man pages'
        msg='Could not remove man pages'
	root=/usr/local/man

        rm -f $root/man1/vefin.1 1>/dev/null 2>&1 || vei_msg_fail "$msg"
        rm -f $root/man1/vefind.1 1>/dev/null 2>&1 || vei_msg_fail "$msg"
        rm -f $root/man3/vefindb.3 1>/dev/null 2>&1 || vei_msg_fail "$msg"
        rm -f $root/man3/vefinu.3 1>/dev/null 2>&1 || vei_msg_fail "$msg"
        rm -f $root/man7/vefintro.7 1>/dev/null 2>&1 || vei_msg_fail "$msg"

	makewhatis || vei_msg_fail "$msg"
}


#
# Removes supplementary documents. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
rm_sup()
{
        vei_msg_info 'Removing supplementary documentation'
        msg='Could not remove supplementary documentation'

        rm -rf /usr/local/share/doc/vefin || vei_msg_fail "$msg"
}


#
# Validates uninstall process has succeeded. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
validate_uninstall()
{
        vei_msg_info 'Validating uninstall'
        msg='Uninstall validation failed'

	[ -f /usr/local/bin/vefin ] && vei_msg_fail "$msg"
	[ -f /usr/local/bin/vefind ] && vei_msg_fail "$msg"

	if grep -q '_vefin' /etc/group; then
	        vei_msg_fail "$msg"
	fi

	if grep -q '_vefin' /etc/passwd; then
	        vei_msg_fail "$msg"
	fi

        [ -f /usr/local/man/man1/vefin.1 ] && vei_msg_fail "$msg"
        [ -f /usr/local/man/man1/vefind.1 ] && vei_msg_fail "$msg"
        [ -f /usr/local/man/man3/vefindb.3 ] && vei_msg_fail "$msg"
        [ -f /usr/local/man/man3/vefinu.3 ] && vei_msg_fail "$msg"
        [ -f /usr/local/man/man7/vefintro.7 ] && vei_msg_fail "$msg"

	[ -d /usr/local/share/doc/vefin ] && vei_msg_fail "$msg"

	vei_msg_ok 'Uninstall complete'
}


#
# Main entry point.
#
source_vei
check_root
rm_bin
rm_man
rm_sup
validate_uninstall
