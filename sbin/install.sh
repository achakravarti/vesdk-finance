#! /bin/sh
#
# install.sh -- installation script.
# This file is part of the VE SDK Finance Microservice.
#
# SDPX-License-Identifier: ISC
# Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#


#
# Sources the VE SDK Init message script dependency. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
source_vei()
{
        if [ -f /usr/local/include/vesdk/init/msg.sh ]; then
	        . /usr/local/include/vesdk/init/msg.sh
	else
                echo 2>&1 'FAIL: vesdk-init library not found'
       		exit 1
	fi
}


#
# Checks whether script is being run as root. See also:
#   - id(1)
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
check_root()
{
        if [ "$(id -u)" -ne 0 ]; then
	        vei_msg_fail 'Permission denied; install as root'
	fi
}


#
# Checks whether an existing installation is present. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
check_install()
{
        vei_msg_info 'Checking prior installation'
	msg='Installation exists; run make uninstall first'

        [ -f /usr/local/bin/vefin ] && vei_msg_fail "$msg"
        [ -f /usr/local/bin/vefind ] && vei_msg_fail "$msg"
        [ -f /usr/local/man/man1/vefin.1 ] && vei_msg_fail "$msg"
        [ -f /usr/local/man/man1/vefind.1 ] && vei_msg_fail "$msg"
        [ -f /usr/local/man/man3/vefindb.3 ] && vei_msg_fail "$msg"
        [ -f /usr/local/man/man3/vefinu.3 ] && vei_msg_fail "$msg"
        [ -f /usr/local/man/man7/vefintro.7 ] && vei_msg_fail "$msg"
}


#
# Checks whether the build is complete. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
check_build()
{
        vei_msg_info 'Checking build'
        msg='Build incomplete; run make first'

        [ -f cli/obj/vefin ] || vei_msg_fail "$msg"
        [ -f srv/obj/vefind ] || vei_msg_fail "$msg"
}


#
# Reverts installation in case of an error. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
undo_install()
{
	vei_msg_warn "$1, reverting changes"
	sbin/uninstall.sh
	vei_msg_fail 'Something went wrong, installation failed'
}


#
# Validates SHA256 checksum of two files. See also:
#   - cksum(1)
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
validate_cksum()
{
        msg='Install validation failed'

	[ -f "$1" ] || undo_install "$msg"
	[ -f "$2" ] || undo_install "$msg"

	[ "$(cksum -q -a sha256 "$1")" = "$(cksum -q -a sha256 "$2")" ] \
	    || undo_install "$msg"
}


#
# Validates man page has been installed correctly. See also:
#   - man(1)
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
validate_man()
{
	if ! man "$1" "$2" 1>/dev/null 2>&1; then
	        undo_install 'Install validation failed'
	fi
}


#
# Installs binaries. See also:
#   - groupadd(1)
#   - useradd(1)
#   - vesdk.init.msg(3)
#   - hier(7)
#   - vefintro(7)
#
install_bin()
{
        vei_msg_info 'Installing binaries'
	msg='Could not install binaries'

	cp cli/obj/vefin /usr/local/bin || undo_install "$msg"
	cp srv/obj/vefind /usr/local/bin || undo_install "$msg"

	vei_msg_info 'Updating ownership'
	msg='Could not update ownership'

	groupadd _vefin || undo_install "$msg"
	useradd                              \
	    -c 'VE SDK Finance Microservice' \
	    -d /var/empty                    \
	    -g _vefin                        \
	    -s /sbin/nologin                 \
	    _vefin || undo_install "$msg"
	chown _vefin:_vefin /usr/local/bin/vefind || undo_install "$msg"
}


#
# Installs man pages. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#   - makewhatis(8)
#
install_man()
{
        vei_msg_info 'Installing man pages'
	msg='Could not install man pages'

	m4 cli/vefin.1 | awk 'NF' > tmp
	cp tmp /usr/local/man/man1/vefin.1 || undo_install "$msg"

	m4 srv/vefind.1 | awk 'NF' > tmp
	cp tmp /usr/local/man/man1/vefind.1 || undo_install "$msg"

	m4 db/vefindb.3 | awk 'NF' > tmp
	cp tmp /usr/local/man/man3/vefindb.3 || undo_install "$msg"

	m4 util/vefinu.3 | awk 'NF' > tmp
	cp tmp /usr/local/man/man3/vefinu.3 || undo_install "$msg"

	m4 vefintro.7 | awk 'NF' > tmp
	cp tmp /usr/local/man/man7/vefintro.7 || undo_install "$msg"

	rm -f tmp 1>/dev/null 2>&1
	makewhatis || undo_install "$msg"
}


#
# Installs supplementary documents. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
install_sup()
{
        vei_msg_info 'Installing supplementary docs'
	msg='Could not install supplementary docs'

	dir=/usr/local/share/doc/vefin
	mkdir -p "$dir" || undo_install "$msg"
	cp README LICENSE "$dir" || undo_install "$msg"
}


#
# Validates installation has succeeded. See also:
#   - vesdk.init.msg(3)
#   - vefintro(7)
#
validate_install()
{
        vei_msg_info 'Validating install'
        msg='Install validation failed'

	validate_cksum cli/obj/vefin /usr/local/bin/vefin 
	validate_cksum srv/obj/vefind /usr/local/bin/vefind

	if ! grep -q '_vefin' /etc/group; then
	        undo_install "$msg"
	fi

	if ! grep -q '_vefin' /etc/passwd; then
	        undo_install "$msg"
	fi

	# shellcheck disable=SC2012
	if ! ls -l /usr/local/bin/vefind \
	    | awk '{print $3, $4}'       \
	    | grep -q '_vefin _vefin'; then
	        undo_install "$msg"
	fi

	validate_cksum README /usr/local/share/doc/vefin/README
	validate_cksum LICENSE /usr/local/share/doc/vefin/LICENSE

	m4 cli/vefin.1 | awk 'NF' > tmp
	validate_cksum tmp /usr/local/man/man1/vefin.1
	validate_man 1 vefin

	m4 srv/vefind.1 | awk 'NF' > tmp
	validate_cksum tmp /usr/local/man/man1/vefind.1
	validate_man 1 vefind

	m4 db/vefindb.3 | awk 'NF' > tmp
	validate_cksum tmp /usr/local/man/man3/vefindb.3
	validate_man 3 vefindb

	m4 util/vefinu.3 | awk 'NF' > tmp
	validate_cksum tmp /usr/local/man/man3/vefinu.3
	validate_man 3 vefinu

	m4 vefintro.7 | awk 'NF' > tmp
	validate_cksum tmp /usr/local/man/man7/vefintro.7
	validate_man 7 vefintro

	rm -f tmp 1>/dev/null 2>&1
	vei_msg_ok 'Installation complete'
}


#
# Main entry point.
#
source_vei
check_root
check_install
check_build
install_bin
install_man
install_sup
validate_install
