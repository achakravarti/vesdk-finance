#
# Makefile -- parent makefile.
# This file is part of VE SDK Finance Microservice.
#
# SDPX-License-Identifier: ISC
# Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
#


# See Ch. 6 of Managing Projects with GNU Make, 3rd Edition, O'Reilly


.if exists(config.mk)
.include "config.mk"
.endif

CLI:=	cli
DB:=	db
SRV:=	srv
UTIL:=	util

.PHONY: ${CLI} ${DB} ${SRV} ${UTIL} all clean install test uninstall

all: ${CLI} ${SRV}

${CLI} ${DB} ${SRV} ${UTIL}:
	@mkdir -p $@/obj
	$(MAKE) -C $@ all

${CLI} ${DB} ${SRV}: ${UTIL}
${SRV}: ${DB}

clean:
	@rm -f *.core
	@rm -rf util/obj
	@rm -rf db/obj
	@rm -rf cli/obj
	@rm -rf srv/obj

install:
	@sbin/install.sh

test:
	@sbin/test.sh

uninstall:
	@sbin/uninstall.sh
