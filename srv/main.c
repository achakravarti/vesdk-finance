#include "srv.h"


int
main(int argc, char *argv[])
{
	struct uds _sock, *sock = &_sock;

	uds_new(sock, "/tmp/test-socket");
	uds_exec(sock);
	uds_free(sock);

	return 0;
}
