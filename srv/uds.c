#include <errno.h>      /* errno(3) */
#include <sys/socket.h> /* socket(2) */
#include <stdio.h>      /* perror(3), snprintf(3) */
#include <stdlib.h>     /* exit(3), malloc(3) */
#include <string.h>     /* strlen(3), strncpy(3), strlcpy(3), memset(3) */
#include <unistd.h>     /* close(2), unlink(2) */

#include "srv.h"


/* http://osr507doc.sco.com/en/netguide/dusockT.code_samples.html */

#define ES01 "(S01) UD socket name %s invalid"
#define ES02 "(S02) Failed to remove existing UD socket %s"
#define ES03 "(S03) Failed to create UD socket %s"
#define ES04 "(S04) Failed to bind UD socket %s"
#define ES05 "(S05) Failed to set UD socket %s as passive"
#define ES06 "(S06) Failed to open active UD socket from %s"
#define ES07 "(S07) Failed to read from active UD socket %d"
#define ES08 "(S08) Failed to close active UD socket %d"


static __dead void
fatal(const struct uds *sock, int rc, const char *msg)
{
	char bfr[1024];

        snprintf(bfr, sizeof(bfr), msg, sock->addr.sun_path);
	perror(msg);

	uds_free((struct uds *)sock);
	exit(rc);
}


void
uds_new(struct uds *sock, const char *path)
{
	size_t len;
	
	len = strlen(path);
	if (!len || len > sizeof(sock->addr.sun_path) - 1)
		fatal(sock, 1, ES01);

	if (unlink(path) == -1 && errno != ENOENT)
		fatal(sock, 2, ES02);

	if ((sock->fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
		fatal(sock, 3, ES03);

	memset(&sock->addr, 0, sizeof(struct sockaddr_un));
	sock->addr.sun_family = AF_UNIX;
	str_rawcp(sock->addr.sun_path, path, sizeof(sock->addr.sun_path));

	if (bind(sock->fd, (struct sockaddr *)&sock->addr,
			  sizeof(struct sockaddr_un)))
		fatal(sock, 4, ES04);

	if (listen(sock->fd, 5))
		fatal(sock, 5, ES05);
}


void
uds_free(struct uds *sock)
{
        (void)close(sock->fd);
        (void)unlink(sock->addr.sun_path);
}


void
uds_exec(struct uds *sock)
{
	const size_t SZ = 512 * 1024;
	int fd, rv;
	size_t sz;
	str *bfr, *tmp, *b;

	bfr = str_new_raw(SZ);

	for (;;) {
	        if(__predict_false((fd = accept(sock->fd, 0, 0)) < 0))
			fatal(sock, 6, ES06);

		do {
			if (__predict_false((rv = read(fd, &sz, sizeof(sz)))
					    < 0))
				fatal(sock, 7, ES07);

			if (__predict_false(!sz))
				break;

			if (__predict_true(sz <= SZ)) {
				tmp = NULL;
				b = bfr;
			} else {
				tmp = str_new_raw(sz);
				b = tmp;
			}
				
			if (__predict_false((rv = read(fd, b, sz)) < 0))
				fatal(sock, 7, ES07);
			else if (rv > 0) {
				sleep(10);
				printf("--> %s\n", b);
				write(fd, &rv, sizeof(rv));
			}

			str_free(&tmp);
		} while (rv > 0);

		if (__predict_false(close(fd)))
			fatal(sock, 8, ES08);
	}

	str_free(&bfr);
}
