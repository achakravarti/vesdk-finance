#ifndef VEFIN_SRV_H
#define VEFIN_SRV_H

#include <sys/types.h>  /* unix(4) */
#include <sys/un.h>     /* unix(4) */

#include "../util/util.h"


/*
 * Unix Domain Socket interface
 */

struct uds {
	int                 fd;
	struct sockaddr_un  addr;
};

__BEGIN_DECLS
extern void uds_new(struct uds *, const char *);
extern void uds_free(struct uds *);
extern void uds_exec(struct uds *);
__END_DECLS


#endif /* !VEFIN_SRV_H */
