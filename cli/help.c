#include <stdio.h>  /* puts(3), printf(3), popen(3) */
#include <stdlib.h> /* exit(3) */

#include "cli.h"


void
help_usage(int err)
{
	const char *msg = "\nUsage:\n"
	    " vefin -h\n"
	    " vefin -l\n"
	    " vefin -v\n"
	    " vefin [-jt] [-f filter] [-p page] [-r rank] domain operation"
	    "\n\nSee vefin(1) for detailed documentation.\n";

	if (err) {
		fputs("\nE01: Invalid usage\n", stderr);
		fputs(msg, stderr);
	} else
		puts(msg);
}


void
help_license(void)
{
	printf("\nVE SDK Finance Microservice License\n\n"
            "SPDX-License-Identifier: ISC\n"
	    "Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>"
            "\n\nPermission to use, copy, modify, and distribute this"
            " software for\n"
            "any purpose with or without fee is hereby granted, provided that\n"
            "the above copyright notice and this permission notice appear"
            " in all copies.\n\n"
            "THE SOFTWARE IS PROVIDED \"AS IS\" AND THE AUTHOR DISCLAIMS ALL\n"
            "WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL"
            "\nTHE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR"
            "\nCONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM\n"
            "LOSS OF USE, DATA OR PROFITS, WHETHER IN ACTION OF CONTRACT,"
            "\nNEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN"
	    "\nCONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n");
}


void
help_version(void)
{
	FILE *pipe;
	char commit[128];
	char date[128];

	if ((pipe = popen("git rev-parse HEAD 2>/dev/null", "r"))) {
		fgets(commit, sizeof(commit), pipe);
		if (pclose(pipe))
			goto err;
	}

	if ((pipe = popen("git log -1 --format=%cd 2>/dev/null", "r"))) {
		fgets(date, sizeof(date), pipe);
		if (pclose(pipe))
			goto err;
	}

	fputs("\nVersion: 0.1\nCommit: ", stdout);
	fputs(commit, stdout);
	fputs("Date: ", stdout);
	fputs(date, stdout);

	return;

err:
	fputs("E02: Failed to read version information", stderr);
	exit(2);
}
