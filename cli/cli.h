#ifndef VEFIN_CLI_H
#define VEFIN_CLI_H


#include "../util/util.h"


extern void help_usage(int);
extern void help_license(void);
extern void help_version(void);

struct cmd {
	str *arg_dom;
	str *arg_op;
	int  opt_h;
	int  opt_l;
	int  opt_v;
	int  opt_t;
};


__BEGIN_DECLS
extern struct cmd *cmd_new(int, char **) __malloc;
extern void cmd_free(struct cmd **);
extern void get_lang(void);
__END_DECLS

#endif /* VEFIN_CLI_H */
