#include <sys/time.h>

#include <stdio.h> /* puts(3) */
#include "cli.h"

/* temporary */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define DATA "Half a league, half a league . . ."

void
get_lang(void)
{
	puts("lang get called...");

    int sock;
    struct sockaddr_un server;
    //char buf[1024];


    sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock < 0) {
        perror("opening stream socket");
        exit(1);
    }
    server.sun_family = AF_UNIX;
    strcpy(server.sun_path, "/tmp/test-socket");

    const struct timeval TIMEOUT = {5, 0};
    setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &TIMEOUT, sizeof(TIMEOUT));

    if (connect(sock, (struct sockaddr *) &server, sizeof(struct sockaddr_un)) < 0) {
        close(sock);
        perror("connecting stream socket");
        exit(1);
    }

    size_t len = strlen(DATA) + 1;
    if (write(sock, &len, sizeof(len)) < 0)
	    perror("writing buffer size on stream socket");
    
    if (write(sock, DATA, len) < 0)
        perror("writing on stream socket");

    puts("wrote data to socket");

    int rv, resp;
    if ((rv = read(sock, &resp, sizeof(resp))) < 0)
	    perror("reading response");

    printf("response --> %d\n", resp);
    

    /* const int TIMEOUT = 5; */
    /* int rv, resp; */
    /* struct timeval start, now, diff; */

    /* gettimeofday(&start, NULL); */
    
    /* do { */
    /* 	    if ((rv = read(sock, &resp, sizeof(resp))) > 0) { */
    /* 		    printf("response --> %d\n", resp); */
    /* 		    break; */
    /* 	    } */

    /* 	    gettimeofday(&now, NULL); */
    /* 	    timersub(&now, &start, &diff); */
    /* 	    printf("Elapsed = %d:%d\n", (int)diff.tv_sec, (int)diff.tv_usec); */

    /* 	    if (diff.tv_sec >= TIMEOUT) { */
    /* 		    perror("reading response"); */
    /* 		    break; */
    /* 	    } */
    /* } while (rv <= 0); */

    close(sock);
}
