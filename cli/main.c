#include "cli.h"


int
main(int argc, char *argv[])
{
	struct cmd *c;

	if (!(c = cmd_new(argc, argv))) {
		help_usage(1);
		return 1;
	}

	if (c->opt_h) {
		help_usage(0);
		return 0;
	}

	if (c->opt_l) {
		help_license();
		return 0;
	}

	if (c->opt_v) {
		help_version();
		return 0;
	}

	get_lang();
	return 0;
}
