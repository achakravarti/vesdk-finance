#include <assert.h> /* assert(3) */
#include <stdio.h>  /* fputs(3) */
#include <stdlib.h> /* malloc(3), exit(3) */
#include <unistd.h> /* getopt(3) */

#include "cli.h"


static int validate(struct cmd *ctx, int argc, char **argv);


struct cmd *
cmd_new(int argc, char **argv)
{
	int opt, eflag;
	struct cmd *ctx;

	if (!(ctx = malloc(sizeof *ctx))) {
		fputs("E04: Failed to allocate memory for struct cmd", stderr);
		exit(4);
	}

	ctx->arg_dom = ctx->arg_op = NULL;

	eflag = opterr = 0;
	while ((opt = getopt(argc, argv, "hlvt")) != -1) {
		switch (opt) {
		case 'h':
			ctx->opt_h = 1;
			break;
		case 'l':
			ctx->opt_l = 1;
			break;
		case 'v':
			ctx->opt_v = 1;
			break;
		case 't':
			ctx->opt_t = 1;
			break;
		default:
			eflag = 1;
		}
	}

	if (!eflag) {
		argc -= optind;
		argv += optind;
		return validate(ctx, argc, argv) ? NULL : ctx;
	}

	return NULL;
}


void
cmd_free(struct cmd **ctx)
{
	struct cmd *c;
	
	if (__predict_true(ctx && (c = *ctx))) {
		str_free(&c->arg_dom);
		str_free(&c->arg_op);
		mem_free(c);
		*ctx = NULL;
	}
}


int
validate(struct cmd *ctx, int argc, char **argv)
{
	assert(ctx);

	if (argc < 0)
		return 1;

	if (ctx->opt_h)
		return (ctx->opt_l || ctx->opt_v || ctx->opt_t || argc);

	if (ctx->opt_l)
		return (ctx->opt_h || ctx->opt_v || ctx->opt_t || argc);

	if (ctx->opt_v)
		return (ctx->opt_h || ctx->opt_l || ctx->opt_t || argc);

	if (argc != 2)
		return 1;

	ctx->arg_dom = argv[0];
	ctx->arg_op = argv[1];

	if (str_cmp(ctx->arg_dom, "lang") && str_cmp(ctx->arg_dom, "curr"))
		return 1;
	
	if (str_cmp(ctx->arg_op, "get")
	    && str_cmp(ctx->arg_op, "post")
	    && str_cmp(ctx->arg_op, "patch")
	    && str_cmp(ctx->arg_op, "delete"))
		return 1;

	return 0;
}
