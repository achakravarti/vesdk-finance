#ifndef VEFIN_UTIL_H
#define VEFIN_UTIL_H


#include <sys/types.h> /* Useful extended C types and macros */

#include <assert.h>   /* assert(3) */
#include <errno.h>    /* errno(3) */
#include <inttypes.h> /* uintptr_t */
#include <stdio.h>    /* perror(3) */
#include <stdlib.h>   /* exit(3), malloc(3) */
#include <string.h>   /* memset(3), strlen(3), strcmp(3), strlcpy(3) */


/*
 * Extension macros
 */

#ifndef __dead
#define __dead __attribute__((noreturn))
#endif

#ifndef __malloc
#define __malloc __attribute__((malloc))
#endif

#ifndef __predict_true
#define __predict_true(x) (__builtin_expect(!!(x), 1))
#endif

#ifndef __predict_false
#define __predict_false(x) (__builtin_expect(!!(x), 0))
#endif

#if (defined __cplusplus && !defined __BEGIN_DECLS)
#define __BEGIN_DECLS extern "C" {
#endif

#if (defined __cplusplus && !defined __END_DECLS)
#define __END_DECLS }
#endif


/*
 * Compatibility macros
 */

#if !(defined __OpenBSD__ || defined __FreeBSD__)
#warn "explicit_bzero() not available, falling back to memset()"
#warn "strlcpy() not available, falling back to strncpy()"
#define explicit_bzero(bfr, sz) memset(bfr, 0, sz);
#define strlcpy(dst, src, sz) strncpy(dstk, src, sz - 1)
#endif

/*
 * Error interface
 * See libvefin(3)
 */

#define ERR_L01 "L01 - mem_alloc()"

extern void err_fatal(int, const char *) __dead;


/*
 * Memory interface
 * See libvefin(3)
 */

typedef void mem;

__BEGIN_DECLS
extern mem *mem_new(size_t) __malloc;
extern size_t mem_sz(const mem *);
extern void mem_free(mem *);
__END_DECLS


/*
 * String interface
 * See libvefin(3)
 */


typedef char str;

__BEGIN_DECLS
extern str *str_new(const char *) __malloc;
extern str *str_new_raw(size_t) __malloc;
extern void str_free(str **);
extern int str_cmp(const str *, const char *);
extern size_t str_len(const str *);
extern size_t str_sz(const str *);
extern void str_rawcp(char *, const char *, size_t);
__END_DECLS


#endif /* VEFIN_UTIL_H */
