#include "util.h"


void
err_fatal(int eno, const char *msg)
{
	errno = eno;
	perror(msg);
	exit(eno);
}
