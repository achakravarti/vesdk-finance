#include "util.h"


#define SZHDR 1


mem *
mem_new(size_t sz)
{
	uintptr_t *bfr;
	
	assert(sz);
	if (__predict_false(!(bfr = malloc(sz + SZHDR))))
		err_fatal(ENOMEM, ERR_L01);

	*bfr = sz;
	explicit_bzero(bfr + SZHDR, sz);
	return bfr;
}


size_t
mem_sz(const mem *bfr)
{
	assert(bfr);
	return ((uintptr_t *)bfr)[-SZHDR];
}


void
mem_free(mem *bfr)
{
	void *b;
	
	b = bfr - SZHDR;
	explicit_bzero(b, mem_sz(bfr) + SZHDR);
	free(b);
}
