#include <assert.h> /* assert(3) */
#include <stdio.h>  /* fputs(3) */
#include <stdlib.h> /* malloc(3), exit(3) */
#include <string.h> /* strlen(3), strncpy(3), strlcpy(3), strcmp(3) */

#include "util.h"


str *
str_new(const char *src)
{
	size_t sz;
	str *s;
	
	assert(src);
	sz = strlen(src) + 1;
	s = mem_new(sz);

        str_rawcp(s, src, sz);
	return s;
}


str *
str_new_raw(size_t sz)
{
	str *s;
	
	assert(sz);
	s = mem_new(sz);
	return s;
}


void
str_free(str **s)
{
	if (__predict_true(s && *s)) {
		mem_free(*s);
		*s = NULL;
	}
}


int
str_cmp(const str *s, const char *cmp)
{
	assert(s);
	assert(cmp);
	return strcmp(s, cmp);
}


size_t
str_len(const str *s)
{
	assert(s);
	return strlen(s);
}


size_t
str_sz(const str *s)
{
	assert(s);
	return strlen(s) + 1;
}

void
str_rawcp(char *dst, const char *src, size_t sz)
{
	assert(dst);
	assert(src);
	assert(sz);
        (void)strlcpy(dst, src, sz);
}
